//DISPARADOR DE TABLA PARTICIPANTE
DELIMITER $$
CREATE TRIGGER actualizarParticipante
AFTER UPDATE ON tb_participante
FOR EACH ROW
BEGIN
  IF ((NEW.codigo <> OLD.codigo) OR (NEW.nombres <> OLD.nombres) OR (NEW.apellidos <> OLD.apellidos) OR (NEW.fecha_nacimiento  <> OLD.fecha_nacimiento) OR (NEW.telefono <> OLD.telefono) OR (NEW.direccion <> OLD.direccion) OR (NEW.fecha_chequeo <> OLD.fecha_chequeo) OR (NEW.peso <> OLD.peso) OR (NEW.talla <> OLD.talla) OR (NEW.fecha_regalo <> OLD.fecha_regalo) OR (NEW.estado <> OLD.estado) OR (NEW.id_nivel <> OLD.id_nivel) OR (NEW.id_religion <> OLD.id_religion)) 
    THEN
      INSERT INTO `tb_bit_participante`(`fecha_registro`, `codigo`, `nombres`, `apellidos`, `fecha_nacimiento`, `telefono`, `direccion`, `fecha_chequeo`, `peso`, `talla`, `fecha_regalo`, `estado`, `id_nivel`, `id_religion`) VALUES (now(),OLD.codigo,OLD.nombres,OLD.apellidos,OLD.fecha_nacimiento,OLD.telefono,OLD.direccion,OLD.fecha_chequeo,OLD.peso,OLD.talla,OLD.fecha_regalo,OLD.estado,OLD.id_nivel,OLD.id_religion);
  END IF ;
END$$
DELIMITER ;

//DISPARADOR DE TABLA USUARIO
DELIMITER $$
CREATE TRIGGER `actualizarUsuario` AFTER UPDATE ON `tb_usuario`
 FOR EACH ROW BEGIN
  IF ((NEW.nombres <> OLD.nombres) OR (NEW.apellidos <> OLD.apellidos) OR (NEW.telefono <> OLD.telefono) OR (NEW.email  <> OLD.email) OR (NEW.password <> OLD.password) OR (NEW.imagen <> OLD.imagen) OR (NEW.estado <> OLD.estado) OR (NEW.usuario_creado <> OLD.usuario_creado) OR (NEW.usuario_edicion <> OLD.usuario_edicion) OR (NEW.id_rol <> OLD.id_rol) OR (NEW.id_genero <> OLD.id_genero)) 
    THEN
      INSERT INTO `tb_bit_usuario`(`fecha_registro`, `nombres`, `apellidos`, `telefono`, `email`, `password`, `imagen`, `estado`, `usuario_creado`, `usuario_edicion`, `id_rol`, `id_genero`) VALUES (now(),OLD.nombres,OLD.apellidos,OLD.telefono,OLD.email,OLD.password,OLD.imagen,OLD.estado,OLD.usuario_creado,OLD.usuario_edicion,OLD.id_rol,OLD.id_genero);
  END IF ;
END$$
DELIMITER ;