<footer class="footer pt-1">
        <section class="footer__copy container">
            <div class="footer__social pb-2">
                <a href="#" class="footer__icons"><img src="./images/facebook.svg" class="footer__img"></a>
                <a href="#" class="footer__icons"><img src="./images/twitter.svg" class="footer__img"></a>
                <a href="#" class="footer__icons"><img src="./images/youtube.svg" class="footer__img"></a>
            </div>

            <h3 class="footer__copyright">Copyright &copy; Software elaborado por estudiantes UMG Cobán - 2022</h3>

            <div class="footer__social pb-2">
                <a class="footer__icons"><img src="./images/umg.png" class="footer__imgU"></a>
            </div>
        </section>
    </footer>

    <script src="./js/slider.js"></script>
    <script src="./js/bootstrap.bundle.min.js"></script>
</body>

</html>