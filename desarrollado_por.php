<?php require 'header.php'; ?>

    <header class="heroU">
        <nav class="nav container"></nav>
        <section class="hero__container container">
            <h1 class="hero__title">PROYECTO DESARROLLADO POR:</h1>
            <p class="hero__paragraph">Estudiantes de la Universidad Mariano Gálvez de Guatemala, 
            facultad de Ingeniería en Sistemas de Información y Ciencias de la Computación con
            sede en Cobán, Alta Verapaz.
            </p>
            
        </section>
    </header>

    <main>
        <section class="container about mt-3">
            <h2 class="subtitle">Equipo de Desarrollo - Seminario 2022:</h2>
            <p class="about__paragraph">Product Owner: <b>Carlos Armando Chub Cucul</b> 
            </p>
            <p class="about__paragraph">Developer Full Stack: <b>Kevin Emmanuel Gallego Gómez</b>
            </p>
            <p class="about__paragraph">Scrum Master: <b>Alex Roberto Emmanuel Coy Cucul</b>
            </p>
            <p class="about__paragraph">Diseñador Base de Datos: <b>Edwing Isaac Moran Tilom</b> 
            </p>
            <p class="about__paragraph">Analista de Sistemas: <b>Julio Fernando Uz Tello</b>
            </p>
            
            <br>
            
            <br>
        </section>

    </main>

<?php require 'footer.php'; ?>