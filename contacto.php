<?php require 'header.php'; ?>

    <header class="hero3">
        <nav class="nav container"></nav>
        <section class="hero__container container">
            <h1 class="hero__title">CONTACTO DE CDI EBENEZER</h1>
            <p class="hero__paragraph">Ubicación geográfica y teléfonos</p>
        </section>
    </header>

    <main>
        <section class="container about mt-3">
            <h2 class="subtitle">Ubicación Geográfica</h2>
            <p class="about__paragraph">Iglesia del Nazareno de Aldea Chicojl, San Pedro Carcha, Alta Verapaz</p>
            <br>
            <h2 class="subtitle">Telefonos</h2>
            <p class="about__paragraph">
                Coordinador de CDI EBENEZER: 00000000 - Nery Eduardo Tumax
            </p>
            <br>
            <p class="about__paragraph">
                Coordinador de Correspondencia: 00000000 - Selvin Oswaldo Cucul
            </p>
            <br>
            <br>
        </section>

    </main>

<?php require 'footer.php'; ?>