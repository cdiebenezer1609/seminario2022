<?php require 'header.php'; ?>

    <header class="hero">
        <nav class="nav container"></nav>
        <section class="hero__container container">
            <h1 class="hero__title">CENTRO DE DESARROLLO INTEGRAL EBENEZER</h1>
            <p class="hero__paragraph">Cuando sabemos que estamos conectados con todos los demás, actuar con compasión
                es simplemente lo natural</p>
        </section>
    </header>

    <main>
        <section class="container about mt-3">
            <h2 class="subtitle">¿Por qué el Centro de Desarrollo Integral Ebenezer existe?</h2>
            <p class="about__paragraph">Porque quiere brindar espacios adecuados para que los niños y adolescentes sin
                distinción alguna, desarrollen sus competencias espirituales, afectivas, cognitivas y motoras, a fin de
                que lleven a la realidad su propuesta de vida de calidad que desean tener.</p>
            <br>
            <h2 class="subtitle">¿Cuales son las areas que cubre el centro de desarrollo integral ebenezer?</h2>
            <div class="about__main">
                <article class="about__icons">
                    <img src="./images/cognitivo.png" class="about__icon">
                    <h3 class="about__title">COGNITIVO</h3>
                    <p class="about__paragrah">Procesos mentales que nos permiten recibir, procesar y elaborar la
                        información.</p>
                </article>

                <article class="about__icons">
                    <img src="./images/espiritual.png" class="about__icon">
                    <h3 class="about__title">ESPIRITUAL</h3>
                    <p class="about__paragrah">Es el área enfocada a mejorar la relación con Dios, mediante el estudio
                        de la biblia.</p>
                </article>
            </div>
            <div class="about__main">
                <article class="about__icons">
                    <img src="./images/social.png" class="about__icon">
                    <h3 class="about__title">SOCIAL</h3>
                    <p class="about__paragrah">Servicio a familia de niños y adolescentes para brindar a la satisfaccion
                        de algunas necesidades.</p>
                </article>

                <article class="about__icons">
                    <img src="./images/fisico.png" class="about__icon">
                    <h3 class="about__title">FÍSICO</h3>
                    <p class="about__paragrah">Servicio de chequeo médico para ayudar a personas a llevar una vida
                        saludable.</p>
                </article>
            </div>
        </section>

        <section class="knowledge pt-4 pb-4 mb-4">
            <div class="knowledge__container container">
                <div class="knowledege__texts">
                    <h2 class="subtitle">Sirviendo a niños y adolescentes para un futuro mejor. ¡Que esperas para ser
                        parte de CDI EBENEZER!</h2>
                    <p class="knowledge__paragraph">Hemos venido a este mundo como hermanos; caminemos, pues, dándonos
                        la mano y uno delante de otro.</p>
                </div>

                <figure class="knowledge__picture">
                    <img src="./images/apoyo_niños.jpg" class="knowledge__img">
                </figure>
            </div>
        </section>

        <section class="price container">
            <h2 class="subtitle">UNETE A ESTA GRAN FAMILIA PARA QUE CON PEQUEÑOS ACTOS SE OBTENGAN GRANDES RESULTADOS
            </h2>
        </section>

        <section class="testimony pt-3">
            <div class="testimony__container container">
                <img src="./images/leftarrow.svg" class="testimony__arrow" id="before">

                <section class="testimony__body testimony__body--show" data-id="1">
                    <div class="testimony__texts">
                        <h2 class="subtitle">Mi nombre es Nery Tumax, <span class="testimony__course">Director de CDI
                                EBENEZER</span></h2>
                        <p class="testimony__review">Mi compromiso es apoyar a niños y adolescentes en las cuatro areas
                            mencionadas.</p>
                    </div>

                    <figure class="testimony__picture">
                        <img src="./images/face4.jpg" class="testimony__img">
                    </figure>
                </section>

                <section class="testimony__body" data-id="2">
                    <div class="testimony__texts">
                        <h2 class="subtitle">Mi nombre es Selvin Oswaldo Cucul, <span class="testimony__course">Coordinador de programa</span></h2>
                        <p class="testimony__review">Mi compromiso es organizar las distintas actividades que se llevan a cabo en el centro para el beneficio de los participantes.</p>
                    </div>

                    <figure class="testimony__picture">
                        <img src="./images/face3.jpg" class="testimony__img">
                    </figure>
                </section>


                <img src="./images/rightarrow.svg" class="testimony__arrow" id="next">
            </div>
        </section>

        <section class="container about mt-3">
            <h2 class="subtitle">¿Por qué el Centro de Desarrollo Integral Ebenezer existe?</h2>
            <p class="about__paragraph">Porque quiere brindar espacios adecuados para que los niños y adolescentes sin
                distinción alguna, desarrollen sus competencias espirituales, afectivas, cognitivas y motoras, a fin de
                que lleven a la realidad su propuesta de vida de calidad que desean tener.</p>
            <p class="about__paragraph">--- Albert Schweitzer ---</p>
        </section>
    </main>

<?php require 'footer.php'; ?>