<?php require 'header.php'; ?>

    <header class="hero2">
        <nav class="nav container"></nav>
        <section class="hero__container container">
            <h1 class="hero__title">ACERCA DE CDI EBENEZER</h1>
            <p class="hero__paragraph">Mision, Vision e Historia de la institución</p>
        </section>
    </header>

    <main>
        <section class="container about mt-3">
            <h2 class="subtitle">Historia</h2>
            <p class="about__paragraph">Todo empieza en el año de 2005 donde el personal de la Iglesia del Nazareno de
                la Aldea Chicojl del Municipio de San Pedro Carcha departamento de Alta Verapaz, toma la iniciativa de
                poder tener un programa que beneficiara no únicamente a los miembros de la iglesia si no que llegar a
                toda la comunidad, por tal razón se inicia los contactos con COMPASSION INTERNACIONAL ya que se constató
                que es una institución seria Cristo Céntrica y con principios y valores cristianos que además su idea es
                formar a los niños desde muy temprana edad.
            </p>
            <p class="about__paragraph">
                Por tal razón en el mes de noviembre del 2005 se realiza reunión ordinaria con la junta directiva de la
                iglesia para realizar una solicitud en la que estuvieron presentes el pastor de la iglesia. Carlos
                Cahuec Bin, Mayordomos Víctor Caal, Juan Caal Alberto Ba Coc, Francisco Tzi Chen, Presidente de jóvenes,
                Gavino Cucul, presidente Misionera Cristobal Pop y hermano Abelardo Che, en dicha reunión se realiza un
                memorándum para solicitar a COMPASSION que tome en cuenta a la iglesia para tener una sociedad con ellos
                y que en la comunidad hay muchos familias y niños que necesitan el apoyo.
            </p>
            <p class="about__paragraph">
                El proceso llevo un año de gestiones hasta que COMPASSION acepto la propuesta, por lo que en el año 2007 se inscribieron a los primeros 200 niños y cada vez se suman más niños, se capacita a los niños de forman integral y están en el proyecto de los  3 a 18 años que están divididos por nivel de 3 a 5 años nivel II, de 6 a 8 años nivel II, de 9 a 11 años nivel III, de 12 a 14 años nivel IV, de 15 a 18 años nivel V, se desarrolla cuatro áreas indispensables en los niños que son área cognitivo, área espiritual, área física, área socio-emocional, las actividades que se realizan son: capacitaciones técnicas, reforzamiento, actividades deportivas, chequeos médicos, devocionales, evangelismo, actividades socioculturales y otros.
            </p>
            <br>
            <h2 class="subtitle">Visión</h2>
            <p class="about__paragraph">
                Ser una entidad formadora de buenos líderes que contribuyan al buen desarrollo de la sociedad, familia, iglesia y comunidad.
            </p>
            <br>
            <h2 class="subtitle">Misión</h2>
            <p class="about__paragraph">
                Nuestro compromiso es apoyar y orientar al beneficiario, con principios y valores cristianos, humanos, sociales y culturales.
            </p>
            <br>
            <br>
        </section>

    </main>

<?php require 'footer.php'; ?>