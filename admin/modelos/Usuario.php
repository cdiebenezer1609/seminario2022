<?php 
//incluir la conexion de base de datos
require "../config/Conexion.php";
class Usuario
{
    //implementamos nuestro constructor
    public function __construct()
    {
    }

    //FUNCION DE INSERTAR QUE SE UTILIZA ACTUALMENTE
    public function insertar_user($nombres, $apellidos, $telefono, $email, $clavehash, $imagen, $usuario_creado, $usuario_edicion, $rol, $genero)
    {
        date_default_timezone_set('America/Guatemala');
        $fecha_creado=date('Y-m-d H:i:s');
        $fecha_edicion=date('Y-m-d H:i:s');
        $sql="INSERT INTO tb_usuario (nombres,apellidos,telefono,email,password,imagen,estado, fecha_creado,fecha_edicion,usuario_creado,usuario_edicion,id_rol,id_genero) VALUES ('$nombres','$apellidos','$telefono','$email','$clavehash','$imagen',1,'$fecha_creado','$fecha_edicion','$usuario_creado','$usuario_edicion','$rol','$genero')";
        return ejecutarConsulta($sql);
    }

    //FUNCION DE EDITAR QUE SE UTILIZA ACTUALMENTE
    public function editar_user($id, $nombres, $apellidos, $telefono, $email, $imagen, $usuario_edicion, $rol, $genero)
    {
        date_default_timezone_set('America/Guatemala');
        $fecha_edicion=date('Y-m-d H:i:s');
        $sql="UPDATE tb_usuario SET nombres='$nombres',apellidos='$apellidos',telefono='$telefono',email='$email',imagen='$imagen',fecha_edicion='$fecha_edicion',usuario_edicion='$usuario_edicion',id_rol='$rol' ,id_genero='$genero' WHERE id='$id'";
        return ejecutarConsulta($sql);
    }

    //FUNCION PARA CAMBIAR CLAVE
    public function editar_clave2($idusuario, $clavehash)
    {
        $sql="UPDATE tb_usuario SET password='$clavehash' WHERE id='$idusuario'";
        return ejecutarConsulta($sql);
    }

    //FUNCION PARA BUSCAR CLAVE
    public function mostrar_clave2($idusuario)
    {
        $sql="SELECT id, password FROM tb_usuario WHERE id='$idusuario'";
        return ejecutarConsultaSimpleFila($sql);
    }

    //FUNCION PARA DESACTIVAR USUARIO
    public function desactivar_user($idusuario)
    {
        $sql="UPDATE tb_usuario SET estado='0' WHERE id='$idusuario'";
        return ejecutarConsulta($sql);
    }

    //FUNCION PARA ACTIVAR USUARIO
    public function activar_user($idusuario)
    {
        $sql="UPDATE tb_usuario SET estado='1' WHERE id='$idusuario'";
        return ejecutarConsulta($sql);
    }

    //FUNCION PARA MOSTRAR REGISTRO A EDITAR
    public function mostrar2($idusuario)
    {
        $sql="SELECT * FROM tb_usuario WHERE id='$idusuario'";
        return ejecutarConsultaSimpleFila($sql);
    }

    //FUNCION PARA MOSTRAR REGISTROS CON DATATABLE
    public function listar2()
    {
        $sql="SELECT tb_usuario.id, `nombres`, `apellidos`, `telefono`, `email`, `password`, `imagen`, `estado`, `fecha_creado`, tb_rol.rol FROM `tb_usuario` INNER JOIN tb_rol ON tb_usuario.id_rol = tb_rol.id";
        return ejecutarConsulta($sql);
    }

    //funcion que actualmente no se esta utilizando
    public function cantidad_usuario()
    {
        $sql="SELECT count(*) nombres FROM tb_usuario";
        return ejecutarConsulta($sql);
    }

    //FUNCION PARA INICIAR SESION
    public function iniciar_sis($email, $password)
    {
        $sql="SELECT tb_usuario.id, tb_usuario.nombres,tb_usuario.email, tb_usuario.password, tb_usuario.imagen, tb_usuario.id_rol FROM tb_usuario WHERE tb_usuario.email='$email' AND password='$password' AND estado='1'";
        return ejecutarConsulta($sql);
    }

    public function listar()
    {
        $sql="SELECT id,nombres,apellidos FROM tb_usuario";
        return ejecutarConsulta($sql);
    }

    public function generar_token_seguro($longitud)
    {
        if ($longitud < 4) {
            $longitud = 4;
        }
    
        return bin2hex(random_bytes(($longitud - ($longitud % 2)) / 2));
    }

	public function usuarioExiste($email){
		$sql = "SELECT id FROM tb_usuario WHERE email = '$email' LIMIT 1";
		return ejecutarConsulta($sql);
	}

	public function usuarioToken($email,$token){
		$sql = "UPDATE tb_usuario SET token = '$token' WHERE email = '$email'";
		return ejecutarConsulta($sql);
	}

    public function validarToken($token){
        $sql = "SELECT id FROM tb_usuario WHERE token = '$token'";
		return ejecutarConsulta($sql);
    }

    public function recovery($token, $clavehash)
    {
        $sql="UPDATE tb_usuario SET password='$clavehash',token=null WHERE token='$token'";
        return ejecutarConsulta($sql);
    }
}

 ?>
