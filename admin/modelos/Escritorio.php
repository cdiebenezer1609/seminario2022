<?php 
//incluir la conexion de base de datos
require "../config/Conexion.php";
date_default_timezone_set('America/Guatemala');
class Escritorio
{
    //implementamos nuestro constructor
    public function __construct()
    {
    }

    //funcion que actualmente no se esta utilizando
    public function Inasistencia_nivel_mensual($nivel,$estado)
    {
        $fecha_actual_aux = date("Y-m-d");
        $fecha_actual = date("Y-m-d",strtotime($fecha_actual_aux."+ 1 days")); 
        $fecha_anterior = date("Y-m-d",strtotime($fecha_actual."- 30 days")); 
        $sql="SELECT count(*) fecha FROM `tb_asistencia` INNER JOIN tb_participante ON tb_asistencia.id_participante = tb_participante.id WHERE (fecha BETWEEN '$fecha_anterior' AND '$fecha_actual') AND (tb_participante.id_nivel=$nivel) AND (tb_asistencia.estado=$estado)";
        return ejecutarConsulta($sql);
    }

    public function Inasistencia_nivel_semanal($nivel,$estado)
    {
        $fecha_actual_aux = date("Y-m-d");
        $fecha_actual = date("Y-m-d",strtotime($fecha_actual_aux."+ 1 days")); 
        $fecha_anterior = date("Y-m-d",strtotime($fecha_actual."- 7 days")); 
        $sql="SELECT count(*) fecha FROM `tb_asistencia` INNER JOIN tb_participante ON tb_asistencia.id_participante = tb_participante.id WHERE (fecha BETWEEN '$fecha_anterior' AND '$fecha_actual') AND (tb_participante.id_nivel=$nivel) AND (tb_asistencia.estado=$estado)";
        return ejecutarConsulta($sql);
    }

    public function cumpleaneros_nivel1(){
        $fecha_actual = date("Y-m-d");
        $fecha_anterior = date("Y-m-d",strtotime($fecha_actual."- 7 days"));
        $sql = "SELECT tb_participante.codigo, tb_participante.nombres, tb_participante.apellidos, tb_participante.fecha_nacimiento, tb_nivel.nivel FROM tb_participante INNER JOIN tb_nivel ON tb_participante.id_nivel = tb_nivel.id WHERE (fecha_nacimiento BETWEEN '$fecha_anterior' AND '$fecha_actual') AND (tb_participante.id_nivel=1) AND (tb_participante.estado=1)";
        return ejecutarConsulta($sql);
    }
    public function cumpleaneros_nivel2(){
        $fecha_actual = date("Y-m-d");
        $fecha_anterior = date("Y-m-d",strtotime($fecha_actual."- 7 days"));
        $sql = "SELECT tb_participante.codigo, tb_participante.nombres, tb_participante.apellidos, tb_participante.fecha_nacimiento, tb_nivel.nivel FROM tb_participante INNER JOIN tb_nivel ON tb_participante.id_nivel = tb_nivel.id WHERE (fecha_nacimiento BETWEEN '$fecha_anterior' AND '$fecha_actual') AND (tb_participante.id_nivel=2) AND (tb_participante.estado=1)";
        return ejecutarConsulta($sql);
    }
    public function cumpleaneros_nivel3(){
        $fecha_actual = date("Y-m-d");
        $fecha_anterior = date("Y-m-d",strtotime($fecha_actual."- 30 days"));
        $sql = "SELECT tb_participante.codigo, tb_participante.nombres, tb_participante.apellidos, tb_participante.fecha_nacimiento, tb_nivel.nivel FROM tb_participante INNER JOIN tb_nivel ON tb_participante.id_nivel = tb_nivel.id WHERE (fecha_nacimiento BETWEEN '$fecha_anterior' AND '$fecha_actual') AND (tb_participante.id_nivel=3) AND (tb_participante.estado=1)";
        return ejecutarConsulta($sql);
    }
    public function cumpleaneros_nivel4(){
        $fecha_actual = date("Y-m-d");
        $fecha_anterior = date("Y-m-d",strtotime($fecha_actual."- 7 days"));
        $sql = "SELECT tb_participante.codigo, tb_participante.nombres, tb_participante.apellidos, tb_participante.fecha_nacimiento, tb_nivel.nivel FROM tb_participante INNER JOIN tb_nivel ON tb_participante.id_nivel = tb_nivel.id WHERE (fecha_nacimiento BETWEEN '$fecha_anterior' AND '$fecha_actual') AND (tb_participante.id_nivel=4) AND (tb_participante.estado=1)";
        return ejecutarConsulta($sql);
    }

    public function cumpleaneros_nivel5(){
        $fecha_actual = date("Y-m-d");
        $fecha_anterior = date("Y-m-d",strtotime($fecha_actual."- 7 days"));
        $sql = "SELECT tb_participante.codigo, tb_participante.nombres, tb_participante.apellidos, tb_participante.fecha_nacimiento, tb_nivel.nivel FROM tb_participante INNER JOIN tb_nivel ON tb_participante.id_nivel = tb_nivel.id WHERE (fecha_nacimiento BETWEEN '$fecha_anterior' AND '$fecha_actual') AND (tb_participante.id_nivel=5) AND (tb_participante.estado=1)";
        return ejecutarConsulta($sql);
    }





    public function cumpleaneros_todos(){
        $fecha_actual = date("Y-m-d");
        $fecha_anterior = date("Y-m-d",strtotime($fecha_actual."- 7 days"));
        $sql = "SELECT tb_participante.codigo, tb_participante.nombres, tb_participante.apellidos, tb_participante.fecha_nacimiento, tb_nivel.nivel FROM tb_participante INNER JOIN tb_nivel ON tb_participante.id_nivel = tb_nivel.id WHERE (fecha_nacimiento BETWEEN '$fecha_anterior' AND '$fecha_actual') AND (tb_participante.estado=1)";
        return ejecutarConsulta($sql);
    }
}

 ?>
