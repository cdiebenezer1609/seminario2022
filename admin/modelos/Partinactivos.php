<?php 
//incluir la conexion de base de datos
require "../config/Conexion.php";
class Partinactivos{

//implementamos nuestro constructor
public function __construct(){

}

//FUNCION DE INSERTAR QUE SE UTILIZA ACTUALMENTE
public function insertar_part($codigo,$nombres,$apellidos,$fecha_nacimiento,$telefono,$direccion,$fecha_chequeo,$peso,$talla,$fecha_regalo,$religion){
	date_default_timezone_set('America/Guatemala');
	$fecha_creado=date('Y-m-d H:i:s');
	$fecha_edicion=date('Y-m-d H:i:s');
	$sql="INSERT INTO tb_participante (codigo,nombres,apellidos,fecha_nacimiento,telefono,direccion,fecha_chequeo,peso,talla,fecha_regalo,estado,fecha_creado,fecha_edicion,id_nivel,id_religion) VALUES ('$codigo','$nombres','$apellidos','$fecha_nacimiento','$telefono','$direccion','$fecha_chequeo','$peso','$talla','$fecha_regalo',1,'$fecha_creado','$fecha_edicion','1','$religion')";
	return ejecutarConsulta($sql);
}

//FUNCION DE EDITAR QUE SE UTILIZA ACTUALMENTE
public function editar_part($id,$codigo,$nombres,$apellidos,$fecha_nacimiento,$telefono,$direccion,$fecha_chequeo,$peso,$talla,$fecha_regalo,$nivel,$religion){
	date_default_timezone_set('America/Guatemala');
	$fecha_edicion=date('Y-m-d H:i:s');
	$sql="UPDATE tb_participante SET codigo='$codigo', nombres='$nombres', apellidos='$apellidos', fecha_nacimiento='$fecha_nacimiento', telefono='$telefono', direccion='$direccion', fecha_chequeo='$fecha_chequeo', peso='$peso', talla='$talla', fecha_regalo='$fecha_regalo', fecha_edicion='$fecha_edicion', id_nivel='$nivel', id_religion='$religion' WHERE id='$id'";
	return ejecutarConsulta($sql);

}

//FUNCION PARA CAMBIAR CLAVE
// public function editar_clave2($idusuario,$clavehash){
// 	$sql="UPDATE tb_usuario SET password='$clavehash' WHERE id='$idusuario'";
// 	return ejecutarConsulta($sql);
// }

//FUNCION PARA BUSCAR CLAVE
// public function mostrar_clave2($idusuario){
// 	$sql="SELECT id, password FROM tb_usuario WHERE id='$idusuario'";
// 	return ejecutarConsultaSimpleFila($sql);
// }

//FUNCION PARA DESACTIVAR USUARIO
public function desactivar_part($id){
	$sql="UPDATE tb_participante SET estado='0' WHERE id='$id'";
	return ejecutarConsulta($sql);
}

//FUNCION PARA ACTIVAR USUARIO
public function activar_part($id){
	$sql="UPDATE tb_participante SET estado='1' WHERE id='$id'";
	return ejecutarConsulta($sql);
}

//FUNCION PARA MOSTRAR REGISTRO A EDITAR
public function mostrar2($id){
	$sql="SELECT * FROM tb_participante WHERE id='$id'";
	return ejecutarConsultaSimpleFila($sql);
}

//FUNCION PARA MOSTRAR REGISTROS CON DATATABLE
public function listar2(){
	$sql="SELECT tb_participante.id, tb_participante.codigo, tb_participante.nombres, tb_participante.apellidos, tb_participante.fecha_nacimiento, tb_participante.telefono, tb_participante.direccion, tb_participante.fecha_chequeo, tb_participante.peso, tb_participante.talla, tb_participante.fecha_regalo, tb_nivel.nivel, tb_religion.religion, tb_participante.fecha_creado, tb_participante.fecha_edicion, tb_participante.estado FROM tb_participante INNER JOIN tb_religion ON tb_participante.id_religion = tb_religion.id INNER JOIN tb_nivel ON tb_participante.id_nivel = tb_nivel.id WHERE tb_participante.estado=0";
	return ejecutarConsulta($sql);
}

}

 ?>