<?php
//activamos almacenamiento en el buffer
ob_start();
session_start();
if (!isset($_SESSION['nombres'])) {
  header("Location: login.php");
} else {
  require 'header.php';
?>
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h1 class="box-title">Participantes nivel I <button class="btn btn-success" onclick="mostrarform(true)" id="btnagregar"><i class="fa fa-plus-circle"></i>Agregar</button></h1>
              <div class="box-tools pull-right">

              </div>
            </div>
            <!--box-header-->
            <!--centro-->
            <div class="panel-body table-responsive" id="listadoregistros">
              <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                  <th>Opciones</th>
                  <th>Código</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Fecha Nacimiento</th>
                  <th>Telefono</th>
                  <th>Direccion</th>
                  <th>Fecha/Chequeo</th>
                  <th>Peso</th>
                  <th>Talla</th>
                  <th>Fecha/Regalo</th>
                  <th>Nivel</th>
                  <th>Religion</th>
                  <th>Fecha/Creación</th>
                  <th>Fecha/Edicion</th>
                  <th>Estado</th>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                  <th>Opciones</th>
                  <th>Código</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Fecha Nacimiento</th>
                  <th>Telefono</th>
                  <th>Direccion</th>
                  <th>Fecha/Chequeo</th>
                  <th>Peso</th>
                  <th>Talla</th>
                  <th>Fecha/Regalo</th>
                  <th>Nivel</th>
                  <th>Religion</th>
                  <th>Fecha/Creacion</th>
                  <th>Fecha/Edicion</th>
                  <th>Estado</th>
                </tfoot>
              </table>
            </div>
            <!-- panel para agregar un participante -->
            <div class="panel-body" id="formularioregistros">
              <form action="" name="formulario" id="formulario" method="POST">
                <!-- codigo general -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_codigo">
                  <label for="">Código(*):</label>
                  <input class="form-control" type="text" name="codigo" id="codigo" maxlength="100" placeholder="Código" required>
                </div>
                <!-- nombres -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_nombres">
                  <label for="">Nombres(*):</label>
                  <input class="form-control" type="hidden" name="idusuario" id="idusuario">
                  <input class="form-control" type="text" name="nombres" id="nombres" maxlength="100" placeholder="Nombre" required>
                </div>
                <!-- apellidos -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_apellidos">
                  <label for="">Apellidos(*):</label>
                  <input class="form-control" type="text" name="apellidos" id="apellidos" maxlength="100" placeholder="Apellidos"required>
                </div>
                <!-- fecha nacimiento -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_nacimiento">
                  <label for="">Fecha nacimiento(*): </label>
                  <input class="form-control" type="date" name="nacimiento" id="nacimiento" maxlength="70" placeholder="email"required>
                </div>
                <!-- telefono -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_telefono">
                  <label for="">Telefono(*): </label>
                  <input class="form-control" type="text" name="telefono" id="telefono" maxlength="70" placeholder="Telefono">
                </div>
                <!-- direccion -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_direccion">
                  <label for="">Direccion(*): </label>
                  <input class="form-control" type="text" name="direccion" id="direccion" maxlength="70" placeholder="Direccion">
                </div>
                <!-- fecha chequeo -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_chequeo">
                  <label for="">Fecha ultimo chequeo(*): </label>
                  <input class="form-control" type="date" name="chequeo" id="chequeo" maxlength="70" placeholder="email">
                </div>
                <!-- peso -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_peso">
                  <label for="">Peso en libras(*): </label>
                  <input class="form-control" type="text" name="peso" id="peso" maxlength="70" placeholder="Peso"required>
                </div>
                <!-- talla -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_talla">
                  <label for="">Talla en cm(*): </label>
                  <input class="form-control" type="text" name="talla" id="talla" maxlength="70" placeholder="Talla"required>
                </div>
                <!-- fecha regalo -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_regalo">
                  <label for="">Fecha ultimo regalo(*): </label>
                  <input class="form-control" type="date" name="regalo" id="regalo" maxlength="70" placeholder="email">
                </div>
                <!-- nivel -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="claves">
                  <label for="">Nivel(*): </label>
                  <select name="idnivel" id="idnivel" class="form-control select-picker" required></select>
                </div>
                <!-- religion -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_religion">
                  <label for="">Religion(*):</label>
                  <select name="idreligion" id="idreligion" class="form-control select-picker" required></select>
                </div>
                <!-- botones para guardar y cancelar -->
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
                  <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                </div>
              </form>
            </div>



            <!--modal para cambio de contraseña-->
            <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" style="width: 20% !important;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Cambio de contraseña</h4>
                  </div>
                  <div class="modal-body">
                    <form action="" name="formularioc" id="formularioc" method="POST">
                      <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Contraseña:</label>
                        <input class="form-control" type="text" name="idusuarioc" id="idusuarioc">
                        <input class="form-control" type="password" name="clavec" id="clavec" maxlength="64" placeholder="Contraseña" required>
                      </div>
                      <button class="btn btn-primary" type="submit" id="btnGuardar_clave"><i class="fa fa-save"></i> Guardar</button>
                      <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                    </form>

                    <div class="modal-footer">
                      <button class="btn btn-default" type="button" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!--inicio de modal editar contraseña--->
              <!--fin de modal editar contraseña--->
              <!--fin centro-->
            </div>

          </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <?php

  require 'footer.php';
  ?>
  <script src="scripts/validations.js"></script>
  <script src="scripts/partnivel1.js"></script>
<?php
}

ob_end_flush();
?>