<?php
    //obtenemos el token
    $token = $_GET["token"];
    if (!isset($token)) {
        header("Location: login.php");
    }else{
        session_start();
        require_once "../modelos/Usuario.php";

        $usuario=new Usuario();
        $validar = $usuario->validarToken($token);
        $fetch_aux = $validar->fetch_row();

        //en caso no exista el token en la bd el usuario no podra cambiar contraseña
        //evitando el uso de tokens viejos
        if(!isset($fetch_aux[0])){
            header("Location: login.php");
        }
    }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CDI EBENEZER</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/font-awesome.css">
   
    <!-- Theme style -->
    <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../public/css/blue.css">

  </head>
  <body class="hold-transition login-page">   

    <?php require '../config/Conexion.php'; ?>
    <div class="login-box">
      <div class="login-logo">
       <a href="../../index.php"><b>CDI</b> EBENEZER</a>
      </div><!-- /.recovery-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Recuperar contraseña</p>
        <form method="post" id="frmStartRecovery">
          <input type="hidden" id="token" name="token" value="<?php echo $token;?>" />
          <p>La contraseña debe contener 8 caracteres, 1 caracter en mayuscula, 1 caracter en minuscula, 1 numero y 1 caracter especial</span>
          <br>
          <div class="form-group has-feedback" id="divpass1">
            <input type="password" id="pass1" name="pass1" class="form-control" placeholder="Contraseña">
            <span class="fa fa-key form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback" id="divpass2">
            <input type="password" id="pass2" name="pass2" class="form-control" placeholder="Confirmar contraseña">
            <span class="fa fa-key form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Recuperar</button>
            </div><!-- /.col -->
          </div>
        </form>        

      </div><!-- /.recovery-box-body -->
    </div><!-- /.recovery-box -->

    <!-- jQuery -->
    <script src="../public/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../public/js/bootstrap.min.js"></script>
     <!-- Bootbox -->
    <script src="../public/js/bootbox.min.js"></script>

    <script type="text/javascript" src="scripts/startrecovery.js"></script>


  </body>
</html> 