<?php
//activamos almacenamiento en el buffer
ob_start();
session_start();
if (!isset($_SESSION['nombres'])) {
  header("Location: login.php");
} else {
  require 'header.php';
?>
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h1 class="box-title">Usuarios <button class="btn btn-success" onclick="mostrarform(true)" id="btnagregar"><i class="fa fa-plus-circle"></i>Agregar</button></h1>
              <div class="box-tools pull-right">

              </div>
            </div>
            <!--box-header-->
            <!--centro-->
            <div class="panel-body table-responsive" id="listadoregistros">
              <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                  <th>Opciones</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Telefono</th>
                  <th>Email</th>
                  <th>Foto</th>
                  <th>Fecha/Registro</th>
                  <th>Estado</th>
                  <th>Rol</th>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                  <th>Opciones</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Telefono</th>
                  <th>Email</th>
                  <th>Foto</th>
                  <th>Fecha/Registro</th>
                  <th>Estado</th>
                  <th>Rol</th>
                </tfoot>
              </table>
            </div>
            <!-- panel para agregar un usuario -->
            <div class="panel-body" id="formularioregistros">
              <form action="" name="formulario" id="formulario" method="POST">
                <!-- rol usuario -->
                <div class="form-group col-lg-12 col-md-12 col-xs-12" id="div_rol">
                  <label for="">Rol de usuario(*):</label>
                  <!-- toma el id para asignarle valores al select -->
                  <select name="idrolusuario" id="idrolusuario" class="form-control select-picker"></select>
                </div>
                <!-- genero -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_genero">
                  <label for="">Genero(*):</label>
                  <select name="idgenero" id="idgenero" class="form-control select-picker"></select>
                </div>
                <!-- nombres -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_nombres">
                  <label for="">Nombres(*):</label>
                  <input class="form-control" type="hidden" name="idusuario" id="idusuario">
                  <input class="form-control" type="text" name="nombres" id="nombres" maxlength="100" placeholder="Nombre">
                </div>
                <!-- apellidos -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_apellidos">
                  <label for="">Apellidos(*):</label>
                  <input class="form-control" type="text" name="apellidos" id="apellidos" maxlength="100" placeholder="Apellidos">
                </div>
                <!-- correo -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_email">
                  <label for="">Correo electronico(*): </label>
                  <input class="form-control" type="email" name="email" id="email" maxlength="70" placeholder="email">
                </div>
                <!-- telefono -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="div_telefono">
                  <label for="">Telefono(*): </label>
                  <input class="form-control" type="text" name="telefono" id="telefono" maxlength="70" placeholder="Telefono">
                </div>
                <!-- contraseña -->
                <div class="form-group col-lg-6 col-md-6 col-xs-12" id="claves">
                  <label for="">Contraseña acceso(*): </label>
                  <input class="form-control" type="password" name="pass" id="pass" maxlength="70" placeholder="Contraseña">
                </div>
                <!-- imagen -->
                <div class="form-group col-lg-12 col-md-12 col-xs-12">
                  <label for="">Imagen:</label>
                  <input class="form-control filestyle" data-buttonText="Seleccionar foto" type="file" name="imagen" id="imagen">
                  <input type="hidden" name="imagenactual" id="imagenactual">
                  <img src="" alt="" width="150px" height="120" id="imagenmuestra">
                </div>
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <button class="btn btn-primary" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
                  <button class="btn btn-danger" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                </div>
              </form>
            </div>



            <!--modal para cambio de contraseña-->
            <div class="modal fade" id="getCodeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" style="width: 20% !important;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="cancelarform_clave()">&times;</button>
                    <h4 class="modal-title">Cambio de contraseña</h4>
                  </div>
                  <div class="modal-body">
                    <form action="" name="formularioc" id="formularioc" method="POST">
                      <div class="form-group" id="div_cambioc">
                        <label for="recipient-name" class="col-form-label">Nueva contraseña:</label>
                        <input class="form-control" type="hidden" name="idusuarioc" id="idusuarioc">
                        <input class="form-control" type="password" name="clavec" id="clavec" maxlength="64" placeholder="Contraseña" required>
                      </div>
                      <button class="btn btn-primary" type="submit" id="btnGuardar_clave"><i class="fa fa-save"></i> Guardar</button>
                      <button class="btn btn-danger" type="button" data-dismiss="modal" onclick="cancelarform_clave()"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
                    </form>

                    <div class="modal-footer">
                      <button class="btn btn-default" type="button" data-dismiss="modal" onclick="cancelarform_clave()">Cerrar</button>
                    </div>
                  </div>
                </div>
              </div>
              <!--inicio de modal editar contraseña--->
              <!--fin de modal editar contraseña--->
              <!--fin centro-->
            </div>

          </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <?php

  require 'footer.php';
  ?>
  <script src="scripts/validations.js"></script>
  <script src="scripts/usuario.js"></script>
<?php
}

ob_end_flush();
?>