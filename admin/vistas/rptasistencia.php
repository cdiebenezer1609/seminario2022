<?php
ob_start();
session_start();
if (!isset($_SESSION['nombres'])) {
  header("Location: login.php");
} else {

  require 'header.php';
?>
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h1 class="box-title">Reporte de Asistencia</h1>
              <div class="box-tools pull-right">

              </div>
            </div>
            <!--box-header-->
            <!--centro-->
            <div class="panel-body table-responsive" id="listadoregistros">

              <label>Filtrar datos por:</label>
              <br>
              <br>
              <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
              
                <label>Fecha Inicio</label>
                <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="<?php echo date("Y-m-d"); ?>">
              </div>
              <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <label>Fecha Fin</label>
                <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" value="<?php echo date("Y-m-d"); ?>">
              </div>
              <div class="form-inline col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <label>Participante</label>
                <select name="idcliente" id="idcliente" class="form-control selectpicker" data-live-search="true" required>
                </select>
                <br>
                <br>
                <button class="btn btn-success" onclick="listar_asistencia();">
                  Filtrar datos</button>
                  <button class="btn btn-info" onclick="listar();">
                  Mostrar todo</button>
              </div>

              <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                  <th>Fecha</th>
                  <th>Código</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Estado</th>
                  <th>Nivel</th>
                  <th>Asistencia</th>
                  <th>Observaciones</th>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                  <th>Fecha</th>
                  <th>Código</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Estado</th>
                  <th>Nivel</th>
                  <th>Asistencia</th>
                  <th>Observaciones</th>
                </tfoot>
              </table>

              <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <button class="btn btn-danger" onclick="window.location.href='escritorio.php'"><i class="fa fa-arrow-circle-left"></i>  Volver</button>
                
              </div>
              <!--    <?php echo $_SESSION['idusuario']; ?></p>  -->
            </div>
          </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <?php

  require 'footer.php';
  ?>
  <script src="scripts/rptasistencia.js"></script>
<?php
}

ob_end_flush();
?>