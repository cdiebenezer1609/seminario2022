<?php
ob_start();
session_start();
if (!isset($_SESSION['nombres'])) {
  header("Location: login.php");
} else {

  require 'header.php';

  $fechamax = date("Y-m-d");
?>
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h1 class="box-title">Control de Asistencia</h1>
              <div class="box-tools pull-right">

              </div>
            </div>
            <!--box-header-->
            <!--centro-->
            <div class="panel-body table-responsive" id="listadoregistros">

            <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <label>Fecha: </label>
                <input type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" max="<?= $fechamax;?>" value="<?php echo date("Y-m-d"); ?>">
              </div>

              <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                  <th>Nivel</th>
                  <th>Código</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Se presento</th>
                  <th>No se presento</th>
                  <th>Observación</th>
                  <th>E. Asistencia</th>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                  <th>Nivel</th>
                  <th>Código</th>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Se presento</th>
                  <th>No se presento</th>
                  <th>Observación</th>
                  <th>E. Asistencia</th>
                </tfoot>
              </table>

              <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button class="btn btn-primary" onclick="guardar_asis();"><i class="fa fa-save"></i> Guardar</button>
                <button class="btn btn-danger" onclick="window.location.href='escritorio.php',cancelar_asis();"><i class="fa fa-close"></i> Cancelar</button>
                <!--<button class="btn btn-danger" type="button"><i class="fa fa-close"></i> Cancelar</button>
                <form action="escritorio.php" method="POST">
                  <br>
                  <input class="btn btn-danger" name="cancelar" type="submit" value="Cancelar" class="fa fa-close" /> <br>
                </form> -->
              </div>
          <!--    <?php echo $_SESSION['idusuario']; ?></p>  -->
            </div>
          </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <?php

  require 'footer.php';
  ?>
  <script src="scripts/asistencia.js"></script>
<?php
}

ob_end_flush();
?>