  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <img src="../../images/umg.png"
    width= "30px"
    height= "30px"
    >
    <strong>  Copyright &copy; <a target="_blank" href="../../desarrollado_por.php">Software elaborado por estudiantes UMG Cobán - 2022.</a></strong> Todos los derechos
    reservados.
    


    
  </footer>

<!-- jQuery 3 -->

  <script src="../public/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../public/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../public/js/app.min.js"></script>

    <!-- DATATABLES -->
    <script src="../public/datatables/jquery.dataTables.min.js"></script>    
    <script src="../public/datatables/dataTables.buttons.min.js"></script>
    <script src="../public/datatables/buttons.html5.min.js"></script>
    <script src="../public/datatables/buttons.colVis.min.js"></script>
    <script src="../public/datatables/jszip.min.js"></script>
    <script src="../public/datatables/pdfmake.min.js"></script>
    <script src="../public/datatables/vfs_fonts.js"></script> 

    <script src="../public/js/bootbox.min.js"></script> 
    <script src="../public/js/bootstrap-select.min.js"></script>
    <script src="../public/js/filestyle.min.js"> </script>
    <script src="../public/js/dayjs.min.js"> </script>
    <script src="scripts/backup.js"></script>

</body> 
</html>