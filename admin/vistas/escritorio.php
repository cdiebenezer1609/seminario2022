<?php
//activamos almacenamiento en el buffer
ob_start();
session_start();
if (!isset($_SESSION['nombres'])) {
  header("Location: login.php");
} else {
  require 'header.php';
  require_once('../modelos/Usuario.php');
  require_once('../modelos/Escritorio.php');
  $usuario = new Usuario();
  $rsptan = $usuario->cantidad_usuario();
  $reg = $rsptan->fetch_object();

  //consultar inasistencia nivel 1
  $asistencia1 = new Escritorio();
  $resaux1 = $asistencia1->Inasistencia_nivel_mensual('1', '2');
  $res1 = $resaux1->fetch_object();

  $resaux2 = $asistencia1->Inasistencia_nivel_mensual('2', '2');
  $res2 = $resaux2->fetch_object();

  $resaux3 = $asistencia1->Inasistencia_nivel_mensual('3', '2');
  $res3 = $resaux3->fetch_object();

  $resaux4 = $asistencia1->Inasistencia_nivel_mensual('4', '2');
  $res4 = $resaux4->fetch_object();

  $resaux5 = $asistencia1->Inasistencia_nivel_mensual('5', '2');
  $res5 = $resaux5->fetch_object();

  //consultar inasistencia semanal
  $resaux1_ = $asistencia1->Inasistencia_nivel_semanal('1', '2');
  $res1_ = $resaux1_->fetch_object();

  $resaux2_ = $asistencia1->Inasistencia_nivel_semanal('2', '2');
  $res2_ = $resaux2_->fetch_object();

  $resaux3_ = $asistencia1->Inasistencia_nivel_semanal('3', '2');
  $res3_ = $resaux3_->fetch_object();

  $resaux4_ = $asistencia1->Inasistencia_nivel_semanal('4', '2');
  $res4_ = $resaux4_->fetch_object();

  $resaux5_ = $asistencia1->Inasistencia_nivel_semanal('5', '2');
  $res5_ = $resaux5_->fetch_object();

} ?>
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="panel-body">

            <?php if ($_SESSION['rol_usuario'] == '1') { ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="small-box bg-green">

                  <a href="asistencia.php" class="small-box-footer">
                    <div class="inner">
                      <h5 style="font-size: 20px;">
                        <strong>Lista asistencias </strong>
                      </h5>
                      <p>Módulo</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-list" aria-hidden="true"></i>
                    </div>&nbsp;
                    <div class="small-box-footer">
                      <i class="fa"></i>
                    </div>

                  </a>
                </div>
              </div>
            <?php } ?>
            <?php if ($_SESSION['rol_usuario'] != '1') { ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="small-box bg-green">

                  <a href="asistencia.php" class="small-box-footer">
                    <div class="inner">
                      <h5 style="font-size: 20px;">
                        <strong>Mi lista asistencias </strong>
                      </h5>
                      <p>Módulo</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-list" aria-hidden="true"></i>
                    </div>&nbsp;
                    <div class="small-box-footer">
                      <i class="fa"></i>
                    </div>

                  </a>
                </div>
              </div>
            <?php } ?>

            <?php if ($_SESSION['rol_usuario'] == '1') { ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="small-box bg-orange">
                  <div class="inner">
                    <h4 style="font-size: 20px;">
                      <strong>Participantes: </strong>
                    </h4>
                    <p>Total <?php echo $reg->nombres; ?></p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                  <a href="usuario.php" class="small-box-footer">Agregar <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            <?php } ?>


            <?php if ($_SESSION['rol_usuario'] == '1') { ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="small-box bg-aqua">

                  <a href="rptasistencia.php" class="small-box-footer">
                    <div class="inner">
                      <h5 style="font-size: 20px;">
                        <strong>Reporte de asistencias </strong>
                      </h5>
                      <p>Módulo</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-list" aria-hidden="true"></i>
                    </div>&nbsp;
                    <div class="small-box-footer">
                      <i class="fa"></i>
                    </div>

                  </a>
                </div>
              </div>
            <?php } ?>
            
            <?php if ($_SESSION['rol_usuario'] != '1') {
            ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="small-box bg-aqua">

                  <a href="rptasistencia.php" class="small-box-footer">
                    <div class="inner">
                      <h5 style="font-size: 20px;">
                        <strong>Mi reporte de asistencias </strong>
                      </h5>
                      <p>Módulo</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-list" aria-hidden="true"></i>
                    </div>&nbsp;
                    <div class="small-box-footer">
                      <i class="fa"></i>
                    </div>
                  </a>
                </div>
              </div>
            <?php } ?>

            <!-- total de inasistencias semanales -->
            <?php if ($_SESSION['rol_usuario'] == '1' || $_SESSION['rol_usuario'] == '2') { ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="small-box bg-aqua" style="padding-bottom: 25px;">
                  <div class="inner">
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia mensual de nivel 1: </strong>
                    </h4>
                    <p>Total <?php echo $res1->fecha; ?></p>
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia semanal de nivel 2: </strong>
                    </h4>
                    <p>Total <?php echo $res1_->fecha; ?></p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            <?php } ?>
            <!-- inasistencia mensual de nivel 2 -->
            <?php if ($_SESSION['rol_usuario'] == '1' || $_SESSION['rol_usuario'] == '3') { ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="small-box bg-olive" style="padding-bottom: 25px;">
                  <div class="inner">
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia mensual de nivel 2: </strong>
                    </h4>
                    <p>Total <?php echo $res2->fecha; ?></p>
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia semanal de nivel 2: </strong>
                    </h4>
                    <p>Total <?php echo $res2_->fecha; ?></p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            <?php } ?>
            <!-- inasistencia mensual de nivel 3 -->
            <?php if ($_SESSION['rol_usuario'] == '1' || $_SESSION['rol_usuario'] == '4') { ?>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="small-box bg-purple" style="padding-bottom: 25px;">
                  <div class="inner">
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia mensual de nivel 3: </strong>
                    </h4>
                    <p>Total <?php echo $res3->fecha; ?></p>
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia semanal de nivel 2: </strong>
                    </h4>
                    <p>Total <?php echo $res3_->fecha; ?></p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            <?php } ?>
            <!-- inasistencia mensual de nivel 4 -->
            <?php if ($_SESSION['rol_usuario'] == '1' || $_SESSION['rol_usuario'] == '5') { ?>
              <?php if ($_SESSION['rol_usuario'] == '1') {?>
                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
              <?php }?>
              <?php if ($_SESSION['rol_usuario'] == '5') {?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <?php }?>
                <div class="small-box bg-teal" style="padding-bottom: 25px;">
                  <div class="inner">
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia mensual de nivel 4: </strong>
                    </h4>
                    <p>Total <?php echo $res4->fecha; ?></p>
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia semanal de nivel 2: </strong>
                    </h4>
                    <p>Total <?php echo $res4_->fecha; ?></p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            <?php } ?>
            <!-- inasistencia mensual de nivel 5 -->
            <?php if ($_SESSION['rol_usuario'] == '1' || $_SESSION['rol_usuario'] == '6') { ?>
              <?php if ($_SESSION['rol_usuario'] == '1') {?>
                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
              <?php }?>
              <?php if ($_SESSION['rol_usuario'] == '6') {?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <?php }?>
                <div class="small-box bg-yellow" style="padding-bottom: 25px;">
                  <div class="inner">
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia mensual de nivel 5: </strong>
                    </h4>
                    <p>Total <?php echo $res5->fecha; ?></p>
                    <h4 style="font-size: 20px;">
                      <strong>Inasistencia semanal de nivel 2: </strong>
                    </h4>
                    <p>Total <?php echo $res5_->fecha; ?></p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            <?php } ?>
            <!-- Aca termina lo de las inasistencias -->
            <!--fin centro-->
          </div>
        </div>
      </div>
    </div>

    <!-- /.box -->


    <!-- ACA ENTRA LO DEL LOS CUMPLEAÑEAROS -->
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h1 class="box-title">Cumpleañeros de la semana</h1>
          </div>
          <!--box-header-->
          <!--centro-->
          <div class="panel-body table-responsive" id="listadoregistros">
          <?php if ($_SESSION['rol_usuario'] == '1') {?>
            <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
          <?php }?>
          <?php if ($_SESSION['rol_usuario'] == '2') {?>
            <table id="tbllistado1" class="table table-striped table-bordered table-condensed table-hover">
          <?php }?>
          <?php if ($_SESSION['rol_usuario'] == '3') {?>
            <table id="tbllistado2" class="table table-striped table-bordered table-condensed table-hover">
          <?php }?>
          <?php if ($_SESSION['rol_usuario'] == '4') {?>
            <table id="tbllistado3" class="table table-striped table-bordered table-condensed table-hover">
          <?php }?>
          <?php if ($_SESSION['rol_usuario'] == '5') {?>
            <table id="tbllistado4" class="table table-striped table-bordered table-condensed table-hover">
          <?php }?>
          <?php if ($_SESSION['rol_usuario'] == '6') {?>
            <table id="tbllistado5" class="table table-striped table-bordered table-condensed table-hover">
          <?php }?>
              <thead>
                <th>Codigo</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Fecha Cumpleaños</th>
                <th>Nivel</th>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <th>Codigo</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Fecha Cumpleaños</th>
                <th>Nivel</th>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>

<?php
require 'footer.php';
?>
<script src="scripts/escritorio.js"></script>
<?php
ob_end_flush();
?>