 <?php
  if (strlen(session_id()) < 1)
    session_start();
  ?>
 <!DOCTYPE html>
 <html>

 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>CDI EBENEZER</title>
   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!-- Bootstrap 3 -->
   <link rel="stylesheet" href="../public/css/bootstrap.min.css">
   <!-- Font Awesome -->
   <link rel="stylesheet" href="../public/css/font-awesome.css">
   <!-- Theme style -->
   <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
   <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="../public/css/_all-skins.min.css">

   <!-- DATATABLES -->
   <link rel="stylesheet" type="text/css" href="../public/datatables/jquery.dataTables.min.css">
   <link href="../public/datatables/buttons.dataTables.min.css" rel="stylesheet" />
   <link href="../public/datatables/responsive.dataTables.min.css" rel="stylesheet" />

   <link rel="stylesheet" type="text/css" href="../public/css/bootstrap-select.min.css">

 </head>

 <body class="hold-transition skin-blue sidebar-mini">
   <!-- Load Facebook SDK for JavaScript -->
   <div id="fb-root"></div>

   <!-- Your customer chat code -->
   <div class="fb-customerchat" attribution=setup_tool page_id="280144326139427" theme_color="#0084ff" logged_in_greeting="Hola! deseas compartir algún sistema o descargar ?" logged_out_greeting="Hola! deseas compartir algún sistema o descargar ?">
   </div>
   <div class="wrapper">

     <header class="main-header">
       <!-- Logo -->
       <a href="escritorio.php" class="logo">
         <!-- mini logo for sidebar mini 50x50 pixels -->
         <span class="logo-mini"><b>CDI</b> EBENEZER</span>
         <!-- logo for regular state and mobile devices -->
         <span class="logo-lg"><b>CDI</b> EBENEZER</span>
       </a>
       <!-- Header Navbar: style can be found in header.less -->
       <nav class="navbar navbar-static-top">
         <!-- Sidebar toggle button-->
         <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
           <span class="sr-only">Navegación</span>
         </a>

         <div class="navbar-custom-menu">
           <ul class="nav navbar-nav">

             <li class="dropdown user user-menu">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                 <?php if ($_SESSION['imagen'] != "" || $_SESSION['imagen'] != null) { ?>
                   <img src="../files/usuarios/<?php echo $_SESSION['imagen']; ?>" class="user-image" alt="User Image">
                 <?php } ?>

                 <span class="hidden-xs"><?php echo $_SESSION['email']; ?></span>
               </a>
               <ul class="dropdown-menu">
                 <!-- User image -->
                 <li class="user-header">
                   <?php if ($_SESSION['imagen'] != "" || $_SESSION['imagen'] != null) { ?>
                     <img src="../files/usuarios/<?php echo $_SESSION['imagen']; ?>" class="img-circle" alt="User Image">
                   <?php } ?>

                   <?php if ($_SESSION['imagen'] == "" || $_SESSION['imagen'] == null) { ?>
                    <p style="margin-top: 50px !important; ">
                     <?php echo $_SESSION['email']; ?>
                     <?php if ($_SESSION['rol_usuario'] == 1) { ?>
                      <small>Administrador</small>
                      <?php } ?>

                      <?php if ($_SESSION['rol_usuario'] > 1) { ?>
                      <small>Tutor nivel <?php echo ($_SESSION['rol_usuario']-1)?></small>
                      <?php } ?> 
                   </p>
                    <?php } else{?>
                      <p>
                     <?php echo $_SESSION['email']; ?>
                     <?php if ($_SESSION['rol_usuario'] == 1) { ?>
                      <small>Administrador</small>
                      <?php } ?>

                      <?php if ($_SESSION['rol_usuario'] > 1) { ?>
                      <small>Tutor nivel <?php echo ($_SESSION['rol_usuario']-1)?></small>
                      <?php } ?> 
                   </p>
                      <?php } ?>
                   
                 </li>
                 <!-- Menu Footer-->
                 <li class="user-footer">
                   <div class="pull-left">
                     <a href="#" class="btn btn-default btn-flat">Perfil</a>
                   </div>
                   <div class="pull-right">
                     <a href="../ajax/usuario.php?op=salir" class="btn btn-default btn-flat">Salir</a>
                     <!-- <a href="login.php" class="btn btn-default btn-flat">Salir</a> -->

                   </div>
                 </li>
               </ul>
             </li>
             <!-- Control Sidebar Toggle Button -->

           </ul>
         </div>
       </nav>
     </header>
     <!-- Left side column. contains the logo and sidebar -->
     <aside class="main-sidebar">
       <!-- sidebar: style can be found in sidebar.less -->
       <section class="sidebar">
         <!-- Sidebar user panel -->
         <div class="user-panel" style="height: 70px;">
           <?php if ($_SESSION['imagen'] == "" || $_SESSION['imagen'] == null) { ?>
           <?php } else { ?>
             <div class="pull-left image">
               <!-- imagen de sidebar -->
               <img src="../files/usuarios/<?php echo $_SESSION['imagen']; ?>" class="img-circle" style="width: 50px; height: 50px;" alt="User Image">
             </div>
           <?php } ?>

           <div class="pull-left info">
             <p><?php echo $_SESSION['nombres']; ?></p>
             <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
           </div>
         </div>
         <!-- sidebar menu: : style can be found in sidebar.less -->
         <ul class="sidebar-menu tree" data-widget="tree">
           <li class="header">MENÚ DE NAVEGACIÓN</li>


           <li><a href="escritorio.php"><i class="fa  fa-dashboard (alias)"></i> <span>Escritorio</span></a></li>


           <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-envelope"></i> <span>Mensajes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="mensaje.php"><i class="fa fa-circle-o"></i> Mensaje</a></li>
          </ul>
      </li> -->


           <?php if ($_SESSION['rol_usuario'] == 1) {
            ?>
             <li class="treeview">
               <a href="#">
                 <i class="fa fa-folder"></i> <span>Acceso</span>
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">
                 <li><a href="usuario.php"><i class="fa fa-circle-o"></i> Usuarios</a></li>
               </ul>
             </li>
             <?php } ?>

             <li class="treeview">
               <a href="#">
                 <i class="fa fa-folder"></i> <span>Participantes</span>
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">
               <?php if ($_SESSION['rol_usuario'] == 1  || $_SESSION['rol_usuario'] == 2) { ?>
                <li><a href="partnivel1.php"><i class="fa fa-circle-o"></i> Nivel 1</a></li>
                <?php } ?>
                <?php if ($_SESSION['rol_usuario'] == 1  || $_SESSION['rol_usuario'] == 3) { ?>
                  <li><a href="partnivel2.php"><i class="fa fa-circle-o"></i> Nivel 2</a></li>
                <?php } ?>
                <?php if ($_SESSION['rol_usuario'] == 1  || $_SESSION['rol_usuario'] == 4) { ?>
                  <li><a href="partnivel3.php"><i class="fa fa-circle-o"></i> Nivel 3</a></li>
                <?php } ?>
                <?php if ($_SESSION['rol_usuario'] == 1  || $_SESSION['rol_usuario'] == 5) { ?>
                  <li><a href="partnivel4.php"><i class="fa fa-circle-o"></i> Nivel 4</a></li>
                <?php } ?>
                <?php if ($_SESSION['rol_usuario'] == 1  || $_SESSION['rol_usuario'] == 6) { ?>
                  <li><a href="partnivel5.php"><i class="fa fa-circle-o"></i> Nivel 5</a></li>
                <?php } ?>
                <?php if ($_SESSION['rol_usuario'] == 1) { ?>
                  <li><a href="partinactivos.php"><i class="fa fa-circle-o"></i> Inactivos</a></li>
                <?php } ?>
               </ul>
             </li>
             <?php if ($_SESSION['rol_usuario'] == 1) { ?>
             <li class="treeview">
               <a href="#">
                 <i class="fa fa-folder"></i> <span>Asistencias</span>
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">
                 <li><a href="asistencia.php"><i class="fa fa-circle-o"></i> Asistencia</a></li>
                 <li><a href="rptasistencia.php"><i class="fa fa-circle-o"></i>Reportes</a></li>

               </ul>
             </li>
             <!-- <li class="treeview">
               <a href="#">
                 <i class="fa fa-folder" ></i> <span>Base de Datos</span>
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">
                 <li><button onclick="hacer_backup()" style="background-color: transparent; border: none; color:white;" id="btnagregar"><i class="fa fa-circle-o" style="padding: 10px;"></i>Realizar Backup</button></li>
               </ul>
             </li> -->
             <?php } ?>
           
           <?php if ($_SESSION['rol_usuario'] != '1') {
            ?>
             <li class="treeview">
               <a href="#">
                 <i class="fa fa-folder"></i> <span>Mis Asistencias</span>
                 <span class="pull-right-container">
                   <i class="fa fa-angle-left pull-right"></i>
                 </span>
               </a>
               <ul class="treeview-menu">
                 <li><a href="asistencia.php"><i class="fa fa-circle-o"></i> Asistencia</a></li>
                 <li><a href="rptasistencia.php"><i class="fa fa-circle-o"></i> Reportes</a></li>
               </ul>
             </li>
           <?php } ?>

           <!--  <li><a href="#"><i class="fa fa-question-circle"></i> <span>Ayuda</span><small class="label pull-right bg-yellow">PDF</small></a></li>
      <li><a href="#"><i class="fa  fa-exclamation-circle"></i> <span>Acerca de</span><small class="label pull-right bg-yellow">2022</small></a></li>   
        -->
         </ul>
       </section>
       <!-- /.sidebar -->
     </aside>