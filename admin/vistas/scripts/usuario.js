var tabla;

//FUNCION QUE SE EJECUTA AL INICIO
function init() {
	mostrarform(false);
	mostrarform_clave(false);
	listar();
	$("#formularioc").on("submit", function (c) {
		editar_clave(c);
	})

	$("#formulario").on("submit", function (e) {
		guardaryeditar(e);
	})

	$("#imagenmuestra").hide();

	//FUNCION PARA CARGAR ITEMS A SELECT DE ROL DE USUARIO
	$.post("../ajax/rolusuario.php?op=selectTiporol", function (r) {
		$("#idrolusuario").html(r);
		$('#idrolusuario').selectpicker('refresh');
	});

	//FUNCION PARA CARGAR ITEMS A SELECT DE GENERO DE SUAURIO
	$.post("../ajax/genero.php?op=selectGenero", function (r) {
		$("#idgenero").html(r);
		$('#idgenero').selectpicker('refresh');
	});
	
	validar_contraseña("#clavec", "#div_cambioc")
	validar_select('idrolusuario',"#div_rol");
	validar_select('idgenero',"#div_genero");
	validar_nombres_apellidos("#nombres", "#div_nombres");
	validar_nombres_apellidos("#apellidos", "#div_apellidos");
	validar_correo("#email", "#div_email");
	validar_telefono("#telefono", "#div_telefono");
	validar_contraseña("#pass", "#claves");
}

//FUNCION PARA LIMPIAR CAMPOS
function limpiar() {
	$("#formulario")[0].reset();
	// $("#nombres").val("");
	// $("#apellidos").val("");
	// $("#telefono").val("");
	$("#idrolusuario").selectpicker('refresh');
	$("#idrolusuario").val("");
	$("#idgenero").selectpicker('refresh');
	$("#idgenero").val("");
	var select1 = document.getElementById("idrolusuario");
    var select2 = document.getElementById("idgenero");
	select1.value=0;
	// $("#email").val("");
	// $("#pass").val("");
	
	// $("#imagenmuestra").attr("src", "");
	// $("#imagenactual").val("");
	$("#idusuarioc").val("");
	$("#idusuario").val("");
	$("#clavec").val("");
	remove_clases("#div_rol");
	remove_clases("#div_genero");
	remove_clases("#div_cambioc");
	remove_clases("#div_nombres");
	remove_clases("#div_apellidos");
	remove_clases("#div_email");
	remove_clases("#div_telefono");
	remove_clases("#claves");
	$("#imagenmuestra").hide();

}

//FUNCION PARA CONTROLAR INTERACCION DE FORMULARIO DE AGREGAR USUARIO
function mostrarform(flag) {
	limpiar();
	if (flag) {
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled", false);
		$("#btnagregar").hide();
	} else {
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//FUNCION PARA CONTROLAR INTERACCION DE FORMULARIO DE CAMBIO DE CONTRASEÑA
function mostrarform_clave(flag) {
	limpiar();
	if (flag) {
		$("#listadoregistros").hide();
		$("#formulario_clave").show();
		$("#btnGuardar_clave").prop("disabled", false);
		$("#btnagregar").hide();
	} else {
		$("#listadoregistros").show();
		$("#formulario_clave").hide();
		$("#btnagregar").show();
	}
}

//FUNCION PARA CANCELAR GUARDAR CAMBIOS
function cancelarform() {
	$("#claves").show();
	limpiar();
	mostrarform(false);
}

//BOTON PARA CANCELAR CAMBIO DE CONTRASEÑA
function cancelarform_clave() {
	limpiar();
	mostrarform_clave(false);
	remove_clases("#div_cambioc");
}

//FUNCION PARA MOSTRAR LISTADO DE USUARIOS
function listar() {
	tabla = $('#tbllistado').dataTable({
		"aProcessing": true,//activamos el procedimiento del datatable
		"aServerSide": true,//paginacion y filrado realizados por el server
		dom: 'Bfrtip',//definimos los elementos del control de la tabla
		buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":
		{
			url: '../ajax/usuario.php?op=listar',
			type: "get",
			dataType: "json",
			error: function (e) {
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10,//paginacion
		"order": [[0, "desc"]]//ordenar (columna, orden)
	}).DataTable();
}

//FUNCION PARA AGREGAR Y EDITAR DATOS
function guardaryeditar(e) {
	e.preventDefault();//no se activara la accion predeterminada
	//validaciones con bootbox
	var nombres = $("#nombres").val();
	var apellidos = $("#apellidos").val();
	var idusuario = $("#idusuario").val();
    var select1 = document.getElementById("idrolusuario");
    var select2 = document.getElementById("idgenero");
	var email1 = $("#email").val();
	var pass = $("#pass").val();
	var telefonos = $("#telefono").val();
    if(select1.value==0){
         bootbox.alert("Debe seleccionar un rol");
		return;
    }
    if(select2.value==0){
        bootbox.alert("Debe seleccionar un género");
		return;
    }
    if(required(nombres) || soloLetras(nombres)){
        bootbox.alert("Digite un nombre");
		return;
    }
    if(required(apellidos) || soloLetras(apellidos)){
        bootbox.alert("Digite un apellido");
		return;
    }
    if(required(email1) || email(email1)){
        bootbox.alert("Digite un correo válido");
		return;
    }
    if(required(telefonos) || soloNumeros(telefonos) || minLength(8,telefonos) || maxLength(8,telefonos)){
        bootbox.alert("Digite un numero de telefono válido");
		return;
    }
	if(idusuario==""){
		if(password(pass) || pass==""){
			bootbox.alert("La contraseña debe tener al menos 8 caracteres, una mayúscula, un número y un caracter especial.");
			return;
		}
	}
	//Carga de datos para guardar
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/usuario.php?op=guardaryeditar",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,

		success: function (datos) {
			bootbox.alert(datos);
			mostrarform(false);
			tabla.ajax.reload();
		}
	});
	$("#claves").show();
	$("#imagenmuestra").hide();
	limpiar();
}

//FUNCION PARA CAMBIAR CONTRASEÑA EN SI
function editar_clave(c) {
	c.preventDefault();//no se activara la accion predeterminada
	//validaciones
	var pass = $("#clavec").val();
	if(password(pass)){
		bootbox.alert("La contraseña debe tener al menos 8 caracteres, una mayúscula, un número y un caracter especial.");
		return;
	}
	//$("#btnGuardar_clave").prop("disabled", true);
	var formData = new FormData($("#formularioc")[0]);
	$.ajax({
		url: "../ajax/usuario.php?op=editar_clave",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,

		success: function (datos) {
			bootbox.alert(datos);
			mostrarform_clave(false);
			tabla.ajax.reload();
		}
	});

	limpiar();
	$("#getCodeModal").modal('hide');
}

//FUNCION PARA CARGAR INFORMACION EN VENTANA MODAL PARA EDITAR USUARIO
function mostrar(idusuario) {
	$.post("../ajax/usuario.php?op=mostrar", { idusuario: idusuario },
		function (data, status) {
			data = JSON.parse(data);
			mostrarform(true);
			if ($("#idusuario").val(data.id).length == 0) {
				$("#claves").show();

			} else {
				$("#claves").hide();
			}

			$("#nombres").val(data.nombres);
			$("#apellidos").val(data.apellidos);
			$("#email").val(data.email);
			$("#telefono").val(data.telefono);
			$("#imagenmuestra").show();
			if(data.imagen=="" || data.imagen==null){
				 $("#imagenmuestra").attr("src", "../files/usuarios/Sin-Foto.png");
			}else{
				$("#imagenmuestra").attr("src", "../files/usuarios/" + data.imagen);
			}
			
			$("#imagenactual").val(data.imagen);

			$("#idrolusuario").val(data.id_rol);
			$("#idrolusuario").selectpicker('refresh');

			$("#idgenero").val(data.id_genero);
			$("#idgenero").selectpicker('refresh');

			$("#idusuario").val(data.id);

		});
	// $.post("../ajax/usuario.php?op=permisos&id=" + idusuario, function (r) {
	// 	$("#permisos").html(r);
	// });
}

//FUNCION PARA MOSTRAR VENTANA MODAL DE CAMBIO DE CONTRASEÑA
function mostrar_clave(idusuario) {
	$("#getCodeModal").modal('show');
	$.post("../ajax/usuario.php?op=mostrar_clave", { idusuario: idusuario },
		function (data, status) {
			data = JSON.parse(data);
			$("#idusuarioc").val(data.id);
		});
}

//FUNCION PARA DESACTIVAR USUARIO
function desactivar(idusuario) {
	bootbox.confirm("¿Esta seguro de desactivar este dato?", function (result) {
		if (result) {
			$.post("../ajax/usuario.php?op=desactivar", { idusuario: idusuario }, function (e) {
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

//FUNCION PARA ACTIVAR USUARIO
function activar(idusuario) {
	bootbox.confirm("¿Esta seguro de activar este dato?", function (result) {
		if (result) {
			$.post("../ajax/usuario.php?op=activar", { idusuario: idusuario }, function (e) {
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

init();