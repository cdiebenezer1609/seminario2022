var tabla;

//funcion que se ejecuta al inicio
function init() {
	listar();

	$("#formulario").on("submit", function () {
		guardaryeditar();

	})
	//cargamos los items al select cliente
	//    $.post("../ajax/asistencia.php?op=selectPersona", function(r){
	//    	$("#idcliente").html(r);
	//    	$('#idcliente').selectpicker('refresh');
	//    });

}

//funcion listar en la tabla los participantes
function listar() {
	tabla = $('#tbllistado').dataTable({
		"aProcessing": true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":
		{
			url: '../ajax/asistencia.php?op=listar',
			type: "get",
			dataType: "json",
			error: function (e) {
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10,
		"order": [[0, "desc"]]
	}).DataTable();
}

function desactivar(idcodigo) {
	$.post("../ajax/asistencia.php?op=desactivar", { idcodigo: idcodigo });
	tabla.ajax.reload();
}

function activar(idcodigo) {
	$.post("../ajax/asistencia.php?op=activar", { idcodigo: idcodigo });
	tabla.ajax.reload();
}

function activar2(idcodigo) {
	//$.post("../ajax/asistencia.php?op=activar2", { idcodigo: idcodigo });
	let observacion = "";
	bootbox.confirm({
		message: "¿Desea agregar una observación?",
		buttons: {
			confirm: {
				label: 'Si'
			},
			cancel: {
				label: 'No'
			}
		},
		callback: function (result) {
			if (result) {
				//Se abre el cuadro de dialogo de observacion
				bootbox.prompt({
					title: "Agregue su observación?",
					buttons: {
						confirm: {
							label: 'Guardar'
						},
						cancel: {
							label: 'Cancelar'
						}
					},
					callback: function (result) {
						if (result != "" || result != null) {
							$.post("../ajax/asistencia.php?op=activar2", { idcodigo: idcodigo, observacion: result }, function (e) {
								bootbox.alert(e);
								tabla.ajax.reload();
							});
						}
						else if (result == null) {
							$.post("../ajax/asistencia.php?op=activar2", { idcodigo: idcodigo, observacion: result }, function (e) {
								bootbox.alert(e);
								tabla.ajax.reload();
							});
						}
					}
				});

				// $.post("../ajax/asistencia.php?op=activar2", { idcodigo: idcodigo }, function (e) {
				// 	bootbox.alert(e);
				// 	tabla.ajax.reload();
				// });
				// $.post("../ajax/asistencia.php?op=guardar_asis", { fecha_inicio}, function (e) {
				// 	bootbox.alert(e);
				// 	tabla.ajax.reload();
				// });
			}
			else {
				$.post("../ajax/asistencia.php?op=activar2", { idcodigo: idcodigo, observacion: null }, function (e) {
					bootbox.alert(e);
					tabla.ajax.reload();
				});
			}
		}
	})
}

function guardar_asis() {
	var fecha_inicio = $("#fecha_inicio").val();
	bootbox.confirm("¿Está seguro de guardar la asistencia?", function (result) {
		if (result) {
			$.post("../ajax/asistencia.php?op=guardar_asis", { fecha_inicio }, function (e) {
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

function cancelar_asis() {
	$.post("../ajax/asistencia.php?op=cancelar_asis");
	tabla.ajax.reload();
}


init();