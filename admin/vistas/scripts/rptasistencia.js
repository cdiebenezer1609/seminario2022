var tabla;

//funcion que se ejecuta al inicio
function init(){
	listar();
   
$("#formulario").on("submit",function(){
   	guardaryeditar();
	
   })
    //cargamos los items al select de participantes
   $.post("../ajax/rptasistencia.php?op=selectPersonaPa", function(r){
   	$("#idcliente").html(r);
   	$('#idcliente').selectpicker('refresh');
   });

}

function listar(){
	tabla=$('#tbllistado').dataTable({
		"aProcessing": true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdf'
		],
		"ajax":
		{
			url:'../ajax/rptasistencia.php?op=listar',
			type: "get",
			dataType : "json",
			error: function (e) {
				console.log(e.responseText);
			}
		},
		"bDestroy":true,
		"iDisplayLength":10,
		"order":[[0,"desc"]]
	}).DataTable();
}


function listar_asistencia(){
	var  fecha_inicio = $("#fecha_inicio").val();
	 var fecha_fin = $("#fecha_fin").val();
	 var idcliente = $("#idcliente").val();
	if (idcliente==""){ idcliente=0; }
		tabla=$('#tbllistado').dataTable({
			"aProcessing": true,//activamos el procedimiento del datatable
			"aServerSide": true,//paginacion y filrado realizados por el server
			dom: 'Bfrtip',//definimos los elementos del control de la tabla
			buttons: [
					  'copyHtml5',
					  'excelHtml5',
					  'csvHtml5',
					  'pdf'
			],
			"ajax":
			{
				url:'../ajax/rptasistencia.php?op=listar_asistencia',
				data:{fecha_inicio:fecha_inicio, fecha_fin:fecha_fin, idcliente: idcliente},
				type: "get",
				dataType : "json",
				error:function(e){
					console.log(e.responseText);
				}
			},
			"bDestroy":true,
			"iDisplayLength":10,//paginacion
			"order":[[0,"desc"]]//ordenar (columna, orden)
		}).DataTable();
	}

init();