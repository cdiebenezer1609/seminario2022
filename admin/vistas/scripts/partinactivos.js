var tabla;

//FUNCION QUE SE EJECUTA AL INICIO
function init() {
	mostrarform(false);
	mostrarform_clave(false);
	listar();
	$("#formularioc").on("submit", function (c) {
		editar_clave(c);
	})

	$("#formulario").on("submit", function (e) {
		guardaryeditar(e);
	})

	//$("#imagenmuestra").hide();

	//FUNCION PARA CARGAR ITEMS A SELECT DE RELIGION
	$.post("../ajax/religion.php?op=selectTiporeligion", function (r) {
		$("#idreligion").html(r);
		$('#idreligion').selectpicker('refresh');
	});

	//FUNCION PARA CARGAR ITEMS A SELECT DE NIVEL
	$.post("../ajax/nivel.php?op=selectNivel", function (r) {
		$("#idnivel").html(r);
		$('#idnivel').selectpicker('refresh');
	});

}

//FUNCION PARA LIMPIAR CAMPOS
function limpiar() {
	$("#codigo").val("");
	$("#nombres").val("");
	$("#apellidos").val("");
	$("#nacimiento").val("");
	$("#telefono").val("");
	$("#direccion").val("");
	$("#chequeo").val("");
	$("#peso").val("");
	$("#talla").val("");
	$("#regalo").val("");
	$("#idreligion").val("");
	$("#idreligion").selectpicker('refresh');
	$("#idnivel").val("");
	$("#idnivel").selectpicker('refresh');
	$("#idusuario").val("");
}

//FUNCION PARA CONTROLAR INTERACCION DE FORMULARIO DE AGREGAR USUARIO
function mostrarform(flag) {
	limpiar();
	if (flag) {
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#btnGuardar").prop("disabled", false);
		$("#btnagregar").hide();
	} else {
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
		
	}
}

//FUNCION PARA CONTROLAR INTERACCION DE FORMULARIO DE CAMBIO DE CONTRASEÑA
function mostrarform_clave(flag) {
	limpiar();
	if (flag) {
		$("#listadoregistros").hide();
		$("#formulario_clave").show();
		$("#btnGuardar_clave").prop("disabled", false);
		$("#btnagregar").hide();
	} else {
		$("#listadoregistros").show();
		$("#formulario_clave").hide();
		$("#btnagregar").show();
	}
}

//FUNCION PARA CANCELAR GUARDAR CAMBIOS
function cancelarform() {
	limpiar();
	mostrarform(false);
}

//BOTON PARA CANCELAR CAMBIO DE CONTRASEÑA
function cancelarform_clave() {
	limpiar();
	mostrarform_clave(false);
}

//FUNCION PARA MOSTRAR LISTADO DE USUARIOS
async function listar() {
	tabla = await $('#tbllistado').dataTable({
		"aProcessing": true,//activamos el procedimiento del datatable
		"aServerSide": true,//paginacion y filrado realizados por el server
		dom: 'Bfrtip',//definimos los elementos del control de la tabla
		buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
				}
            }
		],
		"ajax":
		{
			url: '../ajax/partinactivos.php?op=listar',
			type: "get",
			dataType: "json",
			error: function (e) {
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10,//paginacion
		"order": [[0, "desc"]]//ordenar (columna, orden)
	}).DataTable();
}

//FUNCION PARA AGREGAR Y EDITAR DATOS
function guardaryeditar(e) {
	e.preventDefault();//no se activara la accion predeterminada 
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);

	$.ajax({
		url: "../ajax/partinactivos.php?op=guardaryeditar",
		type: "POST",
		data: formData,
		contentType: false,
		processData: false,

		success: function (datos) {
			bootbox.alert(datos);
			mostrarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

//FUNCION PARA CAMBIAR CONTRASEÑA EN SI
// function editar_clave(c) {
// 	c.preventDefault();//no se activara la accion predeterminada 
// 	$("#btnGuardar_clave").prop("disabled", true);
// 	var formData = new FormData($("#formularioc")[0]);

// 	$.ajax({
// 		url: "../ajax/usuario.php?op=editar_clave",
// 		type: "POST",
// 		data: formData,
// 		contentType: false,
// 		processData: false,

// 		success: function (datos) {
// 			bootbox.alert(datos);
// 			mostrarform_clave(false);
// 			tabla.ajax.reload();
// 		}
// 	});

// 	limpiar();
// 	$("#getCodeModal").modal('hide');
// }

//FUNCION PARA CARGAR INFORMACION EN VENTANA MODAL PARA EDITAR USUARIO
function mostrar(idusuario) {
	$.post("../ajax/partinactivos.php?op=mostrar", { idusuario: idusuario },
		function (data, status) {
			data = JSON.parse(data);
			mostrarform(true);
			// if ($("#idusuario").val(data.id).length == 0) {
			// 	$("#claves").show();

			// } else {
			// 	$("#claves").hide();
			// }
			//muestra el input de nivel
			//$("#claves").show();
			//carga valores a los distintos input
			$("#codigo").val(data.codigo);
			$("#nombres").val(data.nombres);
			$("#apellidos").val(data.apellidos);
			$("#nacimiento").val(data.fecha_nacimiento);
			$("#telefono").val(data.telefono);
			$("#direccion").val(data.direccion);
			$("#chequeo").val(data.fecha_chequeo);
			$("#peso").val(data.peso);
			$("#talla").val(data.talla);
			$("#regalo").val(data.fecha_regalo);

			$("#idnivel").val(data.id_nivel);
			$("#idnivel").selectpicker('refresh');

			$("#idreligion").val(data.id_religion);
			$("#idreligion").selectpicker('refresh');

			$("#idusuario").val(data.id);

		});
}

//FUNCION PARA MOSTRAR VENTANA MODAL DE CAMBIO DE CONTRASEÑA
// function mostrar_clave(idusuario) {
// 	$("#getCodeModal").modal('show');
// 	$.post("../ajax/usuario.php?op=mostrar_clave", { idusuario: idusuario },
// 		function (data, status) {
// 			data = JSON.parse(data);
// 			$("#idusuarioc").val(data.id);
// 		});
// }

//FUNCION PARA DESACTIVAR USUARIO
function desactivar(id) {
	bootbox.confirm("¿Esta seguro de desactivar este dato?", function (result) {
		if (result) {
			$.post("../ajax/partinactivos.php?op=desactivar", { idusuario: id }, function (e) {
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

//FUNCION PARA ACTIVAR USUARIO
function activar(id) {
	bootbox.confirm("¿Esta seguro de activar este dato?", function (result) {
		if (result) {
			$.post("../ajax/partinactivos.php?op=activar", { idusuario: id }, function (e) {
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

//funcion que no se utiliza actualmente
// function generar(longitud) {
// 	long = parseInt(longitud);
// 	var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789";
// 	var contraseña = "";
// 	for (i = 0; i < long; i++) contraseña += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
// 	$("#codigo_persona").val(contraseña);
// }

init();