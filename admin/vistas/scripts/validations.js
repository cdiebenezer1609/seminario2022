const mayus = new RegExp("^(?=.*[A-Z])");
const special = new RegExp("^(?=.*[!#$&@*])");
const numbers = new RegExp("^(?=.*[0-9])");
const lower = new RegExp("^(?=.*[a-z])");
const len = new RegExp("^(?=.{8,})");

//funciones para validacion
function isEmpty(value) {
  return value === undefined || value === null || value === "";
}

//validacion de ingreso de solo letras
function soloLetras(value) {
  const regularExpression = /^[a-zA-ZÀ-ÿ\s]+$/i;
  if (!regularExpression.test(value)) {
    return true;
  }
  return false;
}
//validacion de solo numero
function soloNumeros(value) {
  const regularExpression = /^[0-9]+$/i;
  if (!regularExpression.test(value)) {
    return true;
  }
  return false;
}

function Numeros_con_decimal(value) {
  const regularExpression = /^[0-9]+([.][0-9]+)?$/i;
  if (!regularExpression.test(value)) {
    return true;
  }
  return false;
}

function email(value) {
  // Validacion del formato de un correo electronico
  if (
    !isEmpty(value) &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
  ) {
    return true;
  }

  return false;
}

//funcion para evaluar longitud de un texto
function minValue(min) {
  return (value) => {
    if (!isEmpty(value) && value < min) {
      return true;
    }

    return false;
  };
}

//funcion para evaluar la longitud maxima de un texto
function maxValue(max) {
  return (value) => {
    if (!isEmpty(value) && value > max) {
      return true;
    }

    return false;
  };
}

//funcion para evaluar un numero dentro de un rango
function rangeValue(min, max) {
  return (value) => {
    if (!isEmpty(value) && (value < min || value > max)) {
      return true;
    }
    return false;
  };
}

//funcion para validar la cantidad de caracteres
function minLength(min, value) {
  if (!isEmpty(value) && value.length < min) {
    return true;
  }
  return false;

}

//funcion para limitar numero de caracteres
function maxLength(max, value) {
  if (!isEmpty(value) && value.length > max) {
    return true;
  }

  return false;
}

//funcion para evaluar que el campo es requerido o no puede estar vacio
function required(value) {
  if (isEmpty(value)) {
    return true;
  }
  return false;
}

//funcion para evaluar la seguridad de la contraseña
function password(value) {
  var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/
  if (!isEmpty(value) && !regex.test(value)) {
    return true;
  }
  return false;
}

function alphanumeric(value) {
  const regularExpression = /^[a-z0-9_\-]+$/i;
  if (!isEmpty(value) && !regularExpression.test(value)) {
    return true;
  }
  return false;
}
function alphanumeric2(value) {
  const regularExpression = /^[a-z0-9_\-\.\s]+$/i;
  if (!isEmpty(value) && !regularExpression.test(value)) {
    return true;
  }
  return false;
}

//validacion de inicio con letra
function iniciarConLetra(value) {
  const regularExpression = /^[a-zA-Z]+$/i;
  if (!isEmpty(value) && !regularExpression.test(value[0])) {
    return true;
  }
  return false;
}
//VALIDACIONES DE COLOR DE INPUT
//Funcion para validar select
function validar_select(etiqueta, contenedor) {
  var select = document.getElementById(etiqueta);
  select.addEventListener('change',
    function () {
      var selectedOption = this.options[select.selectedIndex];

      if (selectedOption.value == 0) {
        $(contenedor).addClass("has-error");
        $(contenedor).removeClass("has-success");
      }
      else {
        $(contenedor).addClass("has-success");
        $(contenedor).removeClass("has-error");
      }
    });
}

//funcion para validar nombres y apellidos
function validar_nombres_apellidos(etiqueta, contenedor) {
  $(etiqueta).on("keyup", function (e) {
    var valor = $(etiqueta).val();

    if (required(valor) || soloLetras(valor)) {
      $(contenedor).addClass("has-error");
      $(contenedor).removeClass("has-success");
    }
    else {
      $(contenedor).addClass("has-success");
      $(contenedor).removeClass("has-error");
    }
  });
}

//funcion para validar contraseña
function validar_contraseña(etiqueta, contenedor) {
  $(etiqueta).on("keyup", function (e) {
    var valor = $(etiqueta).val();

    if (password(valor) || valor == "") {
      $(contenedor).addClass("has-error");
      $(contenedor).removeClass("has-success");
    }
    else {
      $(contenedor).addClass("has-success");
      $(contenedor).removeClass("has-error");
    }
  });
}
//funcion para validar correo
function validar_correo(etiqueta, contenedor) {
  $(etiqueta).on("keyup", function (e) {
    var valor = $(etiqueta).val();

    if (required(valor) || email(valor)) {
      $(contenedor).addClass("has-error");
      $(contenedor).removeClass("has-success");
    }
    else {
      $(contenedor).addClass("has-success");
      $(contenedor).removeClass("has-error");
    }
  });
}
//funcion para validar telefono
function validar_telefono(etiqueta, contenedor) {
  $(etiqueta).on("keyup", function (e) {
    var valor = $(etiqueta).val();

    if (required(valor) || maxLength(8, valor) || minLength(8, valor) || soloNumeros(valor)) {
      $(contenedor).addClass("has-error");
      $(contenedor).removeClass("has-success");
    }
    else {
      $(contenedor).addClass("has-success");
      $(contenedor).removeClass("has-error");
    }
  });
}

//Validacion de fecha correcta
const dateAsDayjs = (value) => {
  if (value && dayjs(value).isValid()) {
    return dayjs(value);
  } else {
    return null;
  }
};

//Fecha para validacion fecha posterior
function fecha_posterior(value) {
  if (
    !isEmpty(value) &&
    dateAsDayjs(value).startOf('day').isAfter(dayjs().startOf('day'))
  ) {
    return true;
  }
  return false;
};
//funcion para validar codigo de participante
function validar_codigo(etiqueta, contenedor) {
  $(etiqueta).on("keyup", function (e) {
    var valor = $(etiqueta).val();

    if (required(valor) || alphanumeric(valor)) {
      $(contenedor).addClass("has-error");
      $(contenedor).removeClass("has-success");
    }
    else {
      $(contenedor).addClass("has-success");
      $(contenedor).removeClass("has-error");
    }
  });
}

//funcion para validar fecha 
function validar_fecha_post(etiqueta, contenedor) {
  $(etiqueta).on("change", function (e) {
    var valor = $(etiqueta).val();

    if (required(valor) || dateAsDayjs(valor) == null || fecha_posterior(valor)) {
      $(contenedor).addClass("has-error");
      $(contenedor).removeClass("has-success");
    }
    else {
      $(contenedor).addClass("has-success");
      $(contenedor).removeClass("has-error");
    }
  });
}

function validar_direccion(etiqueta, contenedor) {
  $(etiqueta).on("keyup", function (e) {
    var valor = $(etiqueta).val();

    if (alphanumeric2(valor)) {
      $(contenedor).addClass("has-error");
      $(contenedor).removeClass("has-success");
    }
    else {
      $(contenedor).addClass("has-success");
      $(contenedor).removeClass("has-error");
    }
  });
}

function validar_decimales(etiqueta, contenedor) {
  $(etiqueta).on("keyup", function (e) {
    var valor = $(etiqueta).val();


    if (Numeros_con_decimal(valor)) {
      if (valor >= 1) {
        $(contenedor).addClass("has-error");
        $(contenedor).removeClass("has-success");
      }
      else {
        $(contenedor).addClass("has-error");
        $(contenedor).removeClass("has-success");
      }
    } else {
      $(contenedor).addClass("has-success");
      $(contenedor).removeClass("has-error");
    }



  });
}

//Validar formulario de login
$("#frmAcceso").on('submit', function (e) {
  e.preventDefault();
  //validar inputs
  var email2 = $("#logina").val();
  var pass = $("#clavea").val();

  // if(required(email2)){
  //     bootbox.alert("Debe llenar el campo de correo electronico");
  //     return;
  // }
  // //validar formato de correo
  // if(email(email2)){
  //     bootbox.alert("Debe ingresar un correo válido");
  //     return;
  // }
  // if(required(pass)){
  //     bootbox.alert("Debe llenar el campo de contraseña");
  //     return;
  // }

})

//validar formulario de registro de usuario

//funcion para quitar clase de validaciones
function remove_clases(contenedor) {
  $(contenedor).removeClass("has-success");
  $(contenedor).removeClass("has-error");
}
