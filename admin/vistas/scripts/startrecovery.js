var validar = $(function() {
    var mayus = new RegExp("^(?=.*[A-Z])");
    var special = new RegExp("^(?=.*[!#$&@*])");
    var numbers = new RegExp("^(?=.*[0-9])");
    var lower = new RegExp("^(?=.*[a-z])");
    var len = new RegExp("^(?=.{8,})");

    $("#pass1").on("keyup", function(e) {
        var pass = $("#pass1").val();

        // if (pass.length <= 0) {
        //     $("#divpass1").removeClass("has-success");
        //     $("#divpass1").removeClass("has-error");
        //     return;
        // }

        if (mayus.test(pass) && special.test(pass) && numbers.test(pass) && lower.test(pass) && len.test(pass)) {
            $("#divpass1").addClass("has-success");
            $("#divpass1").removeClass("has-error");
        } else {
            $("#divpass1").addClass("has-error");
            $("#divpass1").removeClass("has-success");
            e.preventDefault();
        }
    });

    $("#pass2").on("keyup", function(e) {
        if ($("#pass1").val() == $("#pass2").val()) {
            $("#divpass2").addClass("has-success");
            $("#divpass2").removeClass("has-error");
        } else {
            $("#divpass2").addClass("has-error");
            $("#divpass2").removeClass("has-success");
        }
    });
});

$("#frmStartRecovery").on('submit', function(e) {
    e.preventDefault();
    pass1 = $("#pass1").val();
    pass2 = $("#pass2").val();
    token = $("#token").val();

    if ($("#pass1").val() == "" || $("#pass2").val() == "") {
        bootbox.alert("Asegúrate de llenar todos los campos");
    } else {

        if (pass1 != pass2) {
            bootbox.alert("Las contraseñas digitadas no coinciden");
            return 0;
        }

        $.post("../ajax/startrecovery.php", { "token": token, "pass": pass1 },
            function(data) {
                if (data == 'cambio') {
                    $(location).attr("href", "login.php?op=2");
                } else {
                    $(location).attr("href", "login.php?op=3");
                }
            });
    }
})