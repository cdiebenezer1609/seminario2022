$("#frmRecovery").on('submit', function(e) {
    e.preventDefault();
    email = $("#email").val();

    if ($("#email").val() == "") {
        bootbox.alert("Asegúrate de llenar todos los campos");
    } else {
        $.post("../ajax/recovery.php", { "email": email },
            function(data) {
                if(data==''){
                    bootbox.alert({
                        message: "El correo ingresado no se encontró!",
                        callback: function () {
                            $(location).attr("href", "recovery.php");
                        }
                    })
                }
                else if (data != 'null') {
                    $(location).attr("href", "login.php?op=1");
                }
                
            });
    }
})