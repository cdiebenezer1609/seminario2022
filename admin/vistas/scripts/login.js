$(document).ready(function() {
    var URLactual = window.location.toString();

    var r = URLactual.includes("op=1");
    if (r) {
        //bootbox.alert("Email de recuperacion enviado con exito");
        bootbox.alert({
            message: "Email de recuperacion enviado con exito!",
            callback: function () {
                $(location).attr("href", "login.php");
            }
        })
        
    }

    var r = URLactual.includes("op=2");
    if (r) {
        //bootbox.alert("La contraseña fue modificada con exito");
        bootbox.alert({
            message: "La contraseña fue modificada con exito!",
            callback: function () {
                $(location).attr("href", "login.php");
            }
        })
    }

    var r = URLactual.includes("op=3");
    if (r) {
        //bootbox.alert("Error en el servidor");
        bootbox.alert({
            message: "Error en el servidor!",
            callback: function () {
                $(location).attr("href", "login.php");
            }
        })
    }
});

$("#frmAcceso").on('submit', function(e) {
    e.preventDefault();
    logina = $("#logina").val();
    clavea = $("#clavea").val();

    if(required(logina)){
        bootbox.alert("Debe llenar el campo de correo electronico");
        return;
    }
    //validar formato de correo
    if(email(logina)){
        bootbox.alert("Debe ingresar un correo válido");
        return;
    }

    if(required(clavea)){
        bootbox.alert("Debe llenar el campo de contraseña");
        
    

    // if ($("#logina").val() == "" || $("#clavea").val() == "") {
    //     bootbox.alert("Asegúrate de llenar todo los campos");
    } else {

        $.post("../ajax/usuario.php?op=verificar", { "logina": logina, "clavea": clavea },
            function(data) {
                if (data != 'null') {
                    $(location).attr("href", "escritorio.php");
                } else {
                    bootbox.alert("Usuario y/o Password incorrectos");
                }
            });
    }
})