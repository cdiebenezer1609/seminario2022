<?php 
session_start();
require_once "../modelos/Partnivel5.php";

$partnivel5=new Partnivel5();

//VARIABLES PARA EL MANEJO DEL CRUD
$idusuario=isset($_POST["idusuario"])? limpiarCadena($_POST["idusuario"]):"";

$codigo=isset($_POST["codigo"])? limpiarCadena($_POST["codigo"]):"";
$nombres=isset($_POST["nombres"])? limpiarCadena($_POST["nombres"]):"";
$apellidos=isset($_POST["apellidos"])? limpiarCadena($_POST["apellidos"]):"";
$nacimiento=isset($_POST["nacimiento"])? limpiarCadena($_POST["nacimiento"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$direccion=isset($_POST["direccion"])? limpiarCadena($_POST["direccion"]):"";
$chequeo=isset($_POST["chequeo"])? limpiarCadena($_POST["chequeo"]):"";
$peso=isset($_POST["peso"])? limpiarCadena($_POST["peso"]):"";
$talla=isset($_POST["talla"])? limpiarCadena($_POST["talla"]):"";
$regalo=isset($_POST["regalo"])? limpiarCadena($_POST["regalo"]):"";
$idnivel=isset($_POST["idnivel"])? limpiarCadena($_POST["idnivel"]):"";
$idreligion=isset($_POST["idreligion"])? limpiarCadena($_POST["idreligion"]):"";

switch ($_GET["op"]) {
	//OPCION PARA GUARDAR Y EDITAR
	case 'guardaryeditar':
		//registrar participante
		if (empty($idusuario)) {
			$idusuario=$_SESSION["idusuario"];
			
			$rspta=$partnivel5->insertar_part($codigo,$nombres,$apellidos,$nacimiento,$telefono,$direccion,$chequeo,$peso,$talla,$regalo,$idreligion);
			echo $rspta ? "Datos registrados correctamente" : "No se pudo registrar todos los datos del usuario";
		}
		//editar participante
		else {
			//$usuario_edicion=$_SESSION["nombres"];
			$rspta=$partnivel5->editar_part($idusuario,$codigo,$nombres,$apellidos,$nacimiento,$telefono,$direccion,$chequeo,$peso,$talla,$regalo,$idnivel,$idreligion);
			echo $rspta ? "Datos actualizados correctamente" : "No se pudo actualizar los datos";
		}
	break;
	//CASO PARA DESACTIVAR USUARIO
	case 'desactivar':
		$rspta=$partnivel5->desactivar_part($idusuario);
		echo $rspta ? "Datos desactivados correctamente" : "No se pudo desactivar los datos";
	break;
	//CASO PARA ACTIVAR USUARIO
	case 'activar':
		$rspta=$partnivel5->activar_part($idusuario);
		echo $rspta ? "Datos activados correctamente" : "No se pudo activar los datos";
	break;
	//CASO PARA MOSTRAR USUARIO A EDITAR
	case 'mostrar':
		$rspta=$partnivel5->mostrar2($idusuario);
		echo json_encode($rspta);
	break;
	//CASO PARA TABLA CON DATOS DE TODOS LOS USUARIOS
	case 'listar':
		$rspta=$partnivel5->listar2();
		//declaramos un array
		$data=Array();
		//SE ESCRIBE CADA FILA
		while ($reg=$rspta->fetch_object()) {
			$data[]=array(
				"0"=>($reg->estado)?'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->id.')"><i class="fa fa-pencil"></i></button>'.' '.'<button class="btn btn-info btn-xs" onclick="mostrar_clave('.$reg->id.')"><i class="fa fa-key"></i></button>'.' '.'<button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->id.')"><i class="fa fa-close"></i></button>':'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->id.')"><i class="fa fa-pencil"></i></button>'.' '.'<button class="btn btn-info btn-xs" onclick="mostrar_clave('.$reg->id.')"><i class="fa fa-key"></i></button>'.' '.'<button class="btn btn-primary btn-xs" onclick="activar('.$reg->id.')"><i class="fa fa-check"></i></button>',
				"1"=>$reg->codigo,
				"2"=>$reg->nombres,
				"3"=>$reg->apellidos,
				"4"=>$reg->fecha_nacimiento,
				"5"=>$reg->telefono,
				"6"=>$reg->direccion,
				"7"=>$reg->fecha_chequeo,
				"8"=>$reg->peso,
				"9"=>$reg->talla,
				"10"=>$reg->fecha_regalo,
				"11"=>$reg->nivel,
				"12"=>$reg->religion,
				"13"=>$reg->fecha_creado,
				"14"=>$reg->fecha_edicion,
				"15"=>($reg->estado)?'<span class="label bg-green">Activado</span>':'<span class="label bg-red">Desactivado</span>'
				
				);
		}

		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);

	break;
}
?>