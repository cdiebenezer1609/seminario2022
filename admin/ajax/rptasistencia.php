<?php
require_once "../modelos/Asistencia.php";
if (strlen(session_id()) < 1)
	session_start();

$asistencia = new Asistencia();

//$codigo_persona = isset($_POST["codigo_persona"]) ? limpiarCadena($_POST["codigo_persona"]) : "";
//$iddepartamento = isset($_POST["iddepartamento"]) ? limpiarCadena($_POST["iddepartamento"]) : "";
$idparticipante = 0;
$estadop = 1;
$idcodigo = isset($_POST["idcodigo"]) ? limpiarCadena($_POST["idcodigo"]) : "";

switch ($_GET["op"]) {

	case 'selectPersonaPa':
		$rspta = $asistencia->listarPa();
		echo '<option value="0">Mostrar todos los participantes...</option>';
		$idderol = $_SESSION['rol_usuario'];
		$idderoll = $idderol - 1;
		while ($reg = $rspta->fetch_object()) {
			if ($idderoll == $reg->id_nivel) { 
				echo '<option value=' . $reg->id . '>' . $reg->nombres . ' ' . $reg->apellidos . '</option>';
			}
			if ($idderoll == 0) {
			echo '<option value=' . $reg->id . '>' . $reg->nombres . ' ' . $reg->apellidos . '</option>';
			}
		}
		break;

	case 'listar':
		$rspta = $asistencia->listarrpt();
		//declaramos un array
		$data = array();
		$idderol = $_SESSION['rol_usuario'];
		$idderoll = $idderol - 1;
		//$estadop=0;
		while ($reg = $rspta->fetch_object()) {

			if ($idderoll == $reg->id_nivel) {
				$data[] = array(
					"0" => $reg->fecha,
					"1" => $reg->codigo,
					"2" => $reg->nombres,
					"3" => $reg->apellidos,
					"4" => ($reg->estadoactivo) ? '<span class="label bg-blue">Activado</span>' : '<span class="label bg-orange">Desactivado</span>',
					"5" => $reg->id_nivel,
					//"5"=>'<button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button>',
					"6" => (($reg->estado) == 2) ? '<span class="label bg-red">Faltante</span>' : '<span class="label bg-green">Se Presento</span>',
					"7" => (($reg->observaciones) == null || ($reg->observaciones) == '') ? '<span class="label bg-gray">Sin observaciones</span>' : '<span class="label bg-yellow">'.$reg->observaciones.'</span>'
				);
			}
			if ($idderoll == 0) {
				$data[] = array(
					"0" => $reg->fecha,
					"1" => $reg->codigo,
					"2" => $reg->nombres,
					"3" => $reg->apellidos,
					"4" => ($reg->estadoactivo) ? '<span class="label bg-blue">Activado</span>' : '<span class="label bg-orange">Desactivado</span>',
					"5" => $reg->id_nivel,
					//"5"=>'<button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button>',
					"6" => (($reg->estado) == 2) ? '<span class="label bg-red">Faltante</span>' : '<span class="label bg-green">Se Presento</span>',
					"7" => (($reg->observaciones) == null || ($reg->observaciones) == '') ? '<span class="label bg-gray">Sin observaciones</span>' : '<span class="label bg-yellow">'.$reg->observaciones.'</span>'
				);
			}
		}

		$results = array(
			"sEcho" => 1, //info para datatables
			"iTotalRecords" => count($data), //enviamos el total de registros al datatable
			"iTotalDisplayRecords" => count($data), //enviamos el total de registros a visualizar
			"aaData" => $data
		);
		echo json_encode($results);

		break;

	case 'listar_asistencia':
		$fecha_inicio = $_REQUEST["fecha_inicio"];
		$fecha_fin = $_REQUEST["fecha_fin"];
		$codigo_persona = $_REQUEST["idcliente"];
		$rspta;

		if ($codigo_persona == 0) { $rspta = $asistencia->listar_asistencia22($fecha_inicio, $fecha_fin);}
		if ($codigo_persona > 0) { $rspta = $asistencia->listar_asistencia($fecha_inicio, $fecha_fin, $codigo_persona);}
		$data = array();

		$idderol = $_SESSION['rol_usuario'];
		$idderoll = $idderol - 1;

		while ($reg = $rspta->fetch_object()) {

		if ($idderoll == $reg->id_nivel) {
			$data[] = array(
				"0" => $reg->fecha,
				"1" => $reg->codigo,
				"2" => $reg->nombres,
				"3" => $reg->apellidos,
				"4" => ($reg->estadoactivo) ? '<span class="label bg-blue">Activado</span>' : '<span class="label bg-orange">Desactivado</span>',
				"5" => $reg->id_nivel,
				//"5"=>'<button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button>',
				"6" => (($reg->estado) == 2) ? '<span class="label bg-red">Faltante</span>' : '<span class="label bg-green">Se Presento</span>',
				"7" => (($reg->observaciones) == null || ($reg->observaciones) == '') ? '<span class="label bg-gray">Sin observaciones</span>' : '<span class="label bg-yellow">'.$reg->observaciones.'</span>'
			);
		}
		if ($idderoll == 0) {
			$data[] = array(
				"0" => $reg->fecha,
				"1" => $reg->codigo,
				"2" => $reg->nombres,
				"3" => $reg->apellidos,
				"4" => ($reg->estadoactivo) ? '<span class="label bg-blue">Activado</span>' : '<span class="label bg-orange">Desactivado</span>',
				"5" => $reg->id_nivel,
				//"5"=>'<button class="btn btn-danger btn-xs"><i class="fa fa-close"></i></button>',
				"6" => (($reg->estado) == 2) ? '<span class="label bg-red">Faltante</span>' : '<span class="label bg-green">Se Presento</span>',
				"7" => (($reg->observaciones) == null || ($reg->observaciones) == '') ? '<span class="label bg-gray">Sin observaciones</span>' : '<span class="label bg-yellow">'.$reg->observaciones.'</span>'
			);
		}
	}

		$results = array(
			"sEcho" => 1, //info para datatables
			"iTotalRecords" => count($data), //enviamos el total de registros al datatable
			"iTotalDisplayRecords" => count($data), //enviamos el total de registros a visualizar
			"aaData" => $data
		);
		echo json_encode($results);

		break;

//Para mostrar los participantes en la barra de busqueda
	case 'selectPersona':
		require_once "../modelos/Usuario.php";
		$usuario = new Usuario();

		$rspta = $usuario->listar();

		while ($reg = $rspta->fetch_object()) {
			echo '<option value=' . $reg->id . '>' . $reg->nombres . ' ' . $reg->apellidos . '</option>';
		}
		break;
}