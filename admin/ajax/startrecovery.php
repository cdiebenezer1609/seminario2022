<?php 
session_start();
require_once "../modelos/Usuario.php";

$usuario=new Usuario();

$token=$_POST["token"];
$pass=$_POST["pass"];

$validar = $usuario->validarToken($token);
$fetch_aux = $validar->fetch_row();

if(!isset($fetch_aux[0])){
    echo 'null';
}
else{
    $clavehash=hash("SHA256", $pass);
    $rspta=$usuario->recovery($token,$clavehash);
    echo $rspta ?'cambio':'null';
}
?>