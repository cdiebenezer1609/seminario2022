<?php 
session_start();
require_once "../modelos/Escritorio.php";

$escritorio=new Escritorio();

//VARIABLES PARA EL MANEJO DEL CRUD
$codigo=isset($_POST["codigo"])? limpiarCadena($_POST["codigo"]):"";
$nombres=isset($_POST["nombres"])? limpiarCadena($_POST["nombres"]):"";
$apellidos=isset($_POST["apellidos"])? limpiarCadena($_POST["apellidos"]):"";
$nacimiento=isset($_POST["nacimiento"])? limpiarCadena($_POST["nacimiento"]):"";
$nivel=isset($_POST["idnivel"])? limpiarCadena($_POST["idnivel"]):"";

switch ($_GET["op"]) {
	//CASO PARA TABLA CON DATOS DE TODOS LOS CUMPLEAÑEROS
	case 'listar_todos':
		$rspta=$escritorio->cumpleaneros_todos();
		//declaramos un array
		$data=Array();
		//SE ESCRIBE CADA FILA
		while ($reg=$rspta->fetch_object()) {
			$data[]=array(
				"0"=>$reg->codigo,
				"1"=>$reg->nombres,
				"2"=>$reg->apellidos,
				"3"=>$reg->fecha_nacimiento,
				"4"=>$reg->nivel
			);
		}
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);
	break;
    //CASO PARA TABLA CON DATOS DE CUMPLEAÑOS POR NIVEL
	case 'listar_nivel1':
		$rspta=$escritorio->cumpleaneros_nivel1();
		//declaramos un array
		$data=Array();
		//SE ESCRIBE CADA FILA
		while ($reg=$rspta->fetch_object()) {
			$data[]=array(
				"0"=>$reg->codigo,
				"1"=>$reg->nombres,
				"2"=>$reg->apellidos,
				"3"=>$reg->fecha_nacimiento,
				"4"=>$reg->nivel,
			);
		}
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);
	break;
    case 'listar_nivel2':
		$rspta=$escritorio->cumpleaneros_nivel2();
		//declaramos un array
		$data=Array();
		//SE ESCRIBE CADA FILA
		while ($reg=$rspta->fetch_object()) {
			$data[]=array(
				"0"=>$reg->codigo,
				"1"=>$reg->nombres,
				"2"=>$reg->apellidos,
				"3"=>$reg->fecha_nacimiento,
				"4"=>$reg->nivel,
			);
		}
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);
	break;
    case 'listar_nivel3':
		$rspta=$escritorio->cumpleaneros_nivel3();
		//declaramos un array
		$data=Array();
		//SE ESCRIBE CADA FILA
		while ($reg=$rspta->fetch_object()) {
			$data[]=array(
				"0"=>$reg->codigo,
				"1"=>$reg->nombres,
				"2"=>$reg->apellidos,
				"3"=>$reg->fecha_nacimiento,
				"4"=>$reg->nivel,
			);
		}
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);
	break;
    case 'listar_nivel4':
		$rspta=$escritorio->cumpleaneros_nivel4();
		//declaramos un array
		$data=Array();
		//SE ESCRIBE CADA FILA
		while ($reg=$rspta->fetch_object()) {
			$data[]=array(
				"0"=>$reg->codigo,
				"1"=>$reg->nombres,
				"2"=>$reg->apellidos,
				"3"=>$reg->fecha_nacimiento,
				"4"=>$reg->nivel,
			);
		}
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);
	break;
    case 'listar_nivel5':
		$rspta=$escritorio->cumpleaneros_nivel5();
		//declaramos un array
		$data=Array();
		//SE ESCRIBE CADA FILA
		while ($reg=$rspta->fetch_object()) {
			$data[]=array(
				"0"=>$reg->codigo,
				"1"=>$reg->nombres,
				"2"=>$reg->apellidos,
				"3"=>$reg->fecha_nacimiento,
				"4"=>$reg->nivel,
			);
		}
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);
	break;
}
?>