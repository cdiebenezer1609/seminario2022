<?php 
session_start();
require_once "../modelos/Usuario.php";

$usuario=new Usuario();

//VARIABLES PARA EL MANEJO DEL CRUD
$idusuario=isset($_POST["idusuario"])? limpiarCadena($_POST["idusuario"]):"";

$nombres=isset($_POST["nombres"])? limpiarCadena($_POST["nombres"]):"";
$apellidos=isset($_POST["apellidos"])? limpiarCadena($_POST["apellidos"]):"";
$telefono=isset($_POST["telefono"])? limpiarCadena($_POST["telefono"]):"";
$email=isset($_POST["email"])? limpiarCadena($_POST["email"]):"";
$password=isset($_POST["pass"])? limpiarCadena($_POST["pass"]):"";
$imagen=isset($_POST["imagen"])? limpiarCadena($_POST["imagen"]):"";

$usuario_creado=isset($_POST["nombres"])? limpiarCadena($_POST["nombres"]):"";
$usuario_edicion=isset($_POST["nombres"])? limpiarCadena($_POST["nombres"]):"";

$idrol=isset($_POST["idrolusuario"])? limpiarCadena($_POST["idrolusuario"]):"";
$idgenero=isset($_POST["idgenero"])? limpiarCadena($_POST["idgenero"]):"";

$idusuarioc=isset($_POST["idusuarioc"])? limpiarCadena($_POST["idusuarioc"]):"";
$clavec=isset($_POST["clavec"])? limpiarCadena($_POST["clavec"]):"";

switch ($_GET["op"]) {
	//OPCION PARA GUARDAR Y EDITAR
	case 'guardaryeditar':

		if (!file_exists($_FILES['imagen']['tmp_name'])|| !is_uploaded_file($_FILES['imagen']['tmp_name']))  
		{
			$imagen=$_POST["imagenactual"];
		}else
		{
			$ext=explode(".", $_FILES["imagen"]["name"]);
			if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png")
			 {
			   $imagen = round(microtime(true)).'.'. end($ext);
				move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/usuarios/" . $imagen);
		 	}
		}
		//Hash SHA256 para la contraseña--encriptando la contraseña
		$clavehash=hash("SHA256", $password);
		//registrar usuario
		if (empty($idusuario)) {
			$idusuario=$_SESSION["idusuario"];
			$usuario_creado=$_SESSION["nombres"];
			
			$rspta=$usuario->insertar_user($nombres,$apellidos,$telefono,$email,$clavehash,$imagen,$usuario_creado,$usuario_edicion,$idrol,$idgenero);
			echo $rspta ? "Datos registrados correctamente" : "No se pudo registrar todos los datos del usuario";
		}
		//editarusuario
		else {
			$usuario_edicion=$_SESSION["nombres"];
			$rspta=$usuario->editar_user($idusuario,$nombres,$apellidos,$telefono,$email,$imagen,$usuario_edicion,$idrol,$idgenero);
			echo $rspta ? "Datos actualizados correctamente" : "No se pudo actualizar los datos";
		}
	break;
	//CASO PARA DESACTIVAR USUARIO
	case 'desactivar':
		$rspta=$usuario->desactivar_user($idusuario);
		echo $rspta ? "Datos desactivados correctamente" : "No se pudo desactivar los datos";
	break;
	//CASO PARA ACTIVAR USUARIO
	case 'activar':
		$rspta=$usuario->activar_user($idusuario);
		echo $rspta ? "Datos activados correctamente" : "No se pudo activar los datos";
	break;
	//CASO PARA MOSTRAR USUARIO A EDITAR
	case 'mostrar':
		$rspta=$usuario->mostrar2($idusuario);
		echo json_encode($rspta);
	break;
	//CASO PARA EDICION DE CONTRASEÑA
	case 'editar_clave':
		$clavehash=hash("SHA256", $clavec);
		$rspta=$usuario->editar_clave2($idusuarioc,$clavehash);
		echo $rspta ? "Password actualizado correctamente" : "No se pudo actualizar el password";
	break;
	//CASO PARA CARGAR FORMULARIO DE CAMBIO DE CONTRASEÑA
	case 'mostrar_clave':
		$rspta=$usuario->mostrar_clave2($idusuario);
		echo json_encode($rspta);
	break;
	//CASO PARA TABLA CON DATOS DE TODOS LOS USUARIOS
	case 'listar':
		$rspta=$usuario->listar2();
		//declaramos un array
		$data=Array();
		//SE ESCRIBE CADA FILA
		while ($reg=$rspta->fetch_object()) {
			$data[]=array(
				"0"=>($reg->estado)?'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->id.')"><i class="fa fa-pencil"></i></button>'.' '.'<button class="btn btn-info btn-xs" onclick="mostrar_clave('.$reg->id.')"><i class="fa fa-key"></i></button>'.' '.'<button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->id.')"><i class="fa fa-close"></i></button>':'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->id.')"><i class="fa fa-pencil"></i></button>'.' '.'<button class="btn btn-info btn-xs" onclick="mostrar_clave('.$reg->id.')"><i class="fa fa-key"></i></button>'.' '.'<button class="btn btn-primary btn-xs" onclick="activar('.$reg->id.')"><i class="fa fa-check"></i></button>',
				"1"=>$reg->nombres,
				"2"=>$reg->apellidos,
				"3"=>$reg->telefono,
				"4"=>$reg->email,
				// "5"=>"<img src='../files/usuarios/".$reg->imagen."' height='50px' width='50px'>",
				"5"=>($reg->imagen!="")?"<img src='../files/usuarios/".$reg->imagen."' height='50px' width='50px'>":"<img src='../files/usuarios/Sin-Foto.png' height='50px' width='50px'>",
				"6"=>$reg->fecha_creado,
				"7"=>($reg->estado)?'<span class="label bg-green">Activado</span>':'<span class="label bg-red">Desactivado</span>',
				"8"=>$reg->rol
				);
		}

		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);

	break;
	//CASO PARA INICIAR SESION
	case 'verificar':
		//validar si el usuario tiene acceso al sistema
		$logina=$_POST['logina'];
		$clavea=$_POST['clavea'];
		//Hash SHA256 en la contraseña
		$clavehash=hash("SHA256", $clavea);
		//metodo que realiza la consulta hacia la bd
		$rspta=$usuario->iniciar_sis($logina,$clavehash);
		//obtiene el resultado
		$fetch=$rspta->fetch_object();
		if (isset($fetch)) 
		{
			# Declaramos la variables de sesion
			$_SESSION['idusuario']=$fetch->id;
			$id=$fetch->id;
			$_SESSION['nombres']=$fetch->nombres;
			$_SESSION['email']=$fetch->email;
			$_SESSION['password']=$fetch->password;
			$_SESSION['imagen']=$fetch->imagen;
			$_SESSION['rol_usuario']=$fetch->id_rol;

			require "../config/Conexion.php";

			$sql="UPDATE tb_usuario SET iteracion='1' WHERE id='$id'";
			echo $sql; 
	 		ejecutarConsulta($sql);		
		}
		echo json_encode($fetch);
	break;
	//CASO PARA SALIR DE LA SESION
	case 'salir':
			
		// $id=$_SESSION['idusuario'];
		// $sql="UPDATE tb_usuario SET iteracion='0' WHERE id='$id'";
		// echo $sql; 
	 	// ejecutarConsulta($sql);	 		

		//Limpiamos las variables de sesión   
        session_unset();
        //Destruìmos la sesión
        session_destroy();
        //Redireccionamos al login
        header("Location: ../vistas/login.php");
	break;
}
?>