# app-seminario
Proyecto final del curso de seminario

# Pasos para montar el proyecto
## 1 - Instalar XAMPP - version 7.4.29
https://www.apachefriends.org/es/download.html

## 2 - Descarga el proyecto desde github
git clone https://github.com/Carlos-Chub/app-seminario.git

## 3 - Crear la base de datos en phpmyadmin con el nombre
control_asistencia

## 4 - El tipo de cotejamiento para la bd debe ser:
utf8_bin

## 5 - En la carpeta descargada, buscar en una carpeta llamada Backup hay un archivo por nombre:
control_aistencia seguido de numero, Tal archivo deberan importarlo en la base de datos creada para que les cree las tablas

## 6 - Importante mencionar que la carpeta del proyecto debe estar en la direccion de htdocs
## 7 - Usuario de inicio de sesion
    Usuario: cdiebenezer1609@gmail.com
    contraseña: admin

