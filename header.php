<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CDI EBENEZER</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/estilos.css">

</head>

<body>
    <!--Header-->
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container pt-2 pb-2 pl-2">
                <!--Logo-->
                <a href="index.php" class="navbar-brand">CDI EBENEZER</a>
                <!--Boton para el menu movil-->
                <button class="navbar-toggler mr-5" type="button" data-bs-target="#contenidoMenu"
                    data-bs-toggle="collapse" aria-controls="contenidoMenu" aria-expanded="false"
                    aria-label="Mostrar / Ocultar Navegacion">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!--Menu de navegacion-->
                <div id="contenidoMenu" class="collapse navbar-collapse">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <!--Inicio-->
                        <li class="nav-item active ml-auto">
                            <a class="nav-link" href="index.php">Inicio</a>
                        </li>
                        <!--Contacto-->
                        <li class="nav-item">
                            <a class="nav-link" href="acerca_de.php">Acerca De</a>
                        </li>
                        <!--Contacto-->
                        <li class="nav-item">
                            <a class="nav-link" href="contacto.php">Contacto</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="desarrollado_por.php" >Desarrollado por</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="./admin/vistas/login.php" >Iniciar sesión</a>
                        </li>
                        
                        
                    </ul>
                </div>
            </div>
        </nav>
    </header>